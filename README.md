
# Media Library
**Project:** 	Contology
**Framework:**	Codeigniter

## Description
A media library based on the Wordpress media manager. A user is able to upload media and categorize with tags that have  identifiable terms. The system also automatically tags pertinent information, month, date, year to save the user time and allows consistency across all users. 

---

# SEM Budget Manager
**Project:** 	Contology
**Framework:** 	Codeigniter

## Description**
A budget manager to allow the SEM department to manage all client's SEM spending within the web application. We pull SEM spending data from google, bing and facebook apis nightly through a cron job and save in either a xml file or the database depending on the source. 

From there we calculate multiple formulas like daily and monthly spending requirements, based on accounting data within the app. 

###### Notes
The model was omitted because it was built by another team member.

---

# Messaging Component
**Project:** 	Homeschool Hub
**Framework:** 	React

## Description**
This is a working prototype for a messaging system within a learning management system. It was for showing a proof of concept for clients as well as a basis for backend developers to reference in the future, including some mock data structure.  Currently all user actions are stored in state only. 
This app features custom theming, so I used the styled components library to pass in theming data into component css.

---