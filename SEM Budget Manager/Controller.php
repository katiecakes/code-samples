<?php

class Manage extends DOM_Controller
{
    private $client_id;
    private $client_name;
    private $atDOMLevel = false;

    /**
     * Constructor
     *
     * First before loading this controller, a check is made here to verify that we are on the group level with group 56 selected
     * OR we are selected on a client that has access to the SEM module, otherwise we redirect to the dashboard with an error message
     * We also set the activeNav to 'advertising'
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model(array('domsem', 'domsembudget', 'groups', 'domdev', 'domclients'));

        // Make sure that the user has permission ViewAdvertisingManage to access this controller
        if($this->checkPermission('ViewAdvertisingManage',true)==false)
        {
            throwError(newError("", 2, 'Unfortunately, you do not have access to this page.', 1, ''));
            redirect(base_url(), 'refresh');
        } //make sure a client is selected or group id 56 is selected
        elseif ($this->user['DropdownDefault']->SelectedGroup == Groups::DOM) {
            $this->atDOMLevel = true;
            $this->client_id = null;
            $this->client_name = null;
        } elseif ((array_key_exists(4, $this->client_modules) && ($this->user['DropdownDefault']->LevelType == 3 || $this->user['DropdownDefault']->LevelType == 'c'))
            || $this->user['DropdownDefault']->SelectedGroup == Groups::DOM
        ) {
            $this->client_id = $this->user['DropdownDefault']->SelectedClient;
            $this->client_name = $this->domclients->get_client_name($this->client_id);
//make sure client has active sem module and sem keys listed in db
            $active_ads = ($this->domsem->has_sem($this->client_id)) ? true : false;
            if (!$active_ads && ($this->user['DropdownDefault']->SelectedGroup != Groups::DOM || $this->user['DropdownDefault']->LevelType != 'g')) {
                throwError(newError("SEM Reporting Dashboard", 2, 'We are still in the process of setting up the SEM Reporting Dashboard for this client.', 1, ''));
                redirect(base_url(), 'refresh');
            }
        } else {
            throwError(newError("Advertising Dashboard", 2, 'Access Denied to the Advertising Dashboard', 1, ''));
            redirect(base_url(), 'refresh');
        }
        $this->activeNav = 'advertising';
    }

    /**
     * Default function that is loaded when the digital controller is called
     */

    public function Index()
    {
        $data = array(
            'at_dom_level' => $this->atDOMLevel,
            'client_id' => $this->client_id,
            'client_name' => $this->client_name,
        );
        if($this->atDOMLevel == true) {
            $this->LoadTemplate('pages/advertising/manage/dom', $data);
        } else {
            $this->LoadTemplate('pages/advertising/manage/client', $data);
        }
    }

    /*
     * Get_Daily_Client_Spend
     * Used as an ajax endpoint to get spending for a particular client.
     *
     * @input month - Month of data we want the spend data for
     * @input year - Year we want the spend data for
     * @input client_id - Client ID of the client we want the data for this will be used to also verify that the client selected is the active client to avoid forged requests
     *
     * @author KA <kaustin@dom360.com>
     * @version 2016-04-01
     */
    public function get_daily_client_spend(){

        //get post variables and validate input
        $fields = $this->input->post();
        $result = 1;
        $data=[];
        (isset($fields['client_id'])) ? $client_id = $fields['client_id'] : $result = 0;
        (isset($fields['month'])) ? $month = $fields['month'] : $month = date("m");
        (isset($fields['year'])) ? $year = $fields['year'] : $year = date("Y");

        if(($result == 0) || (checkdate($month,'01',$year) === false))
        {
            echo json_encode(array('success' => '0', 'msg'=> 'There is an error with the client supplied', 'fields'=> $fields, 'result'=>checkdate($month,'05',$year) === false));
            exit;
        }
        else
        {
            //initialize variables
            $data = array();

            $this->load->model(array('domalerts', 'domsembudget'));
            $clients = $this->domalerts->get_sem_clients('',$client_id); //get client Info
            $time_data = $this->domsem->get_budget_dates($year,$month); //get exact dates/time/days in the time format needed
            $begin_date = new DateTime($time_data['date_fm']['begin_date']);
            $end_date = new DateTime($time_data['date_fm']['end_date']);
            //Add a day so we can include last day of month in the date period
            $end_date->modify('+1 day');
            $period = new DatePeriod(
                $begin_date,
                new DateInterval('P1D'),
                $end_date
            );
            if(count($clients) >= 1)
            {
                foreach($clients as $client)
                {
                    $working_budget = $admin_fee_amount = $so_far = 0;
                    $current_day = 1;
                    $budget = $this->domsem->get_monthly_sem_budget($client_id,$time_data['time_fm']['begin_date'], $time_data['time_fm']['end_date']);
                    $data['client_id'] = $client['client_id'];
                    $data['client_name'] = $client['client_name'];
                    $data['client_code'] = $client['client_code'];
                    $data['begin_date'] = $time_data['date_fm']['begin_date'];
                    $data['end_date'] = $time_data['date_fm']['end_date'];
                    $data['days'] = $time_data['time_fm']['days'];
                    $data['total_budget'] = $budget['total_budget'];
                    $data['admin_type'] = $budget['admin_type'];
                    $data['admin_fee'] = $budget['admin_fee'];
                    $data['carry_over'] = $budget['carry_over'];
                    $data['period'] = $period;
                    $rollovers = $this->domsembudget->get_rollover($client['client_id'], $month, $year);
                    $fees = $this->domsembudget->get_fees($client['client_id'], $month, $year);
                    $payments = $this->domsembudget->get_payments($client['client_id'], $month, $year);
                    $tot_fee = $tot_rollover = $tot_pay = 0;
                    foreach ($rollovers as $r){
                        if($r['trans_type'] == 2){
                            $tot_rollover += $r['amount'];
                        } else if($r['trans_type'] == 4){
                            $tot_rollover -= $r['amount'];
                        }
                    }
                    foreach ($fees as $f){
                        $tot_fee += $f['amount'];
                    }
                    foreach ($payments as $p) {
                        $tot_pay += $p['amount'];
                    }
                    $admin_fee_amount = $tot_fee;
                    $working_budget = $tot_pay + $tot_rollover - $tot_fee;
                    /*if($budget['admin_type'] == '1') {
                        $admin_fee_amount = $budget['total_budget'] * ($budget['admin_fee'] * .01);
                        $working_budget = $budget['total_budget'] - ($admin_fee_amount) + $budget['carry_over'];
                    } elseif ($budget['admin_type'] == '2') {
                        $admin_fee_amount = $budget['admin_fee'];
                        $working_budget = $budget['total_budget'] - ($admin_fee_amount) + $budget['carry_over'];
                    } elseif ($budget['admin_type'] == '0') {
                        $working_budget = $budget['total_budget'] + $budget['carry_over'];
                    }*/
                    $data['admin_fee_amount'] = $admin_fee_amount;
                    $data['working_budget'] = $working_budget;
                    $data['daily_budget'] = number_format(($working_budget / $time_data['date_fm']['days']), 2, '.', '');

                    $daily = $this->domsem->get_client_spend_data($client_id,$time_data['date_fm']['begin_date'], $time_data['date_fm']['end_date']);
                    $spend = [];
                    foreach ($daily as $d) {
                        $dt = date('m/d/Y', strtotime($d['date']));
                        $total_spent = $spend_per = $left_to_spend = 0;

                        $spend[$dt]['id'] = $d['id'];
                        $spend[$dt]['date'] = date('m/d/Y', strtotime($d['date']));
                        $spend[$dt]['day'] = $current_day;
                        $spend[$dt]['google'] = $d['google_spend'];
                        $spend[$dt]['bing'] = $d['bing_spend'];
                        $spend[$dt]['facebook'] = $d['fb_spend'];

                        if(is_numeric($d['google_spend'])) { $total_spent += $d['google_spend']; }
                        if(is_numeric($d['bing_spend'])) { $total_spent += $d['bing_spend']; }
                        if(is_numeric($d['fb_spend'])) { $total_spent += $d['fb_spend']; }
                        $daily_target = ($working_budget - $so_far) / ($time_data['time_fm']['days'] - $current_day+1);
                        $spend[$dt]['should_be'] = ($working_budget/$time_data['time_fm']['days']) * $current_day;
                        $so_far += $total_spent;
                        $spend[$dt]['so_far'] = $so_far;
                        $spend_per = number_format(($total_spent / $data['daily_budget']) * 100, 0);
                        $spend[$dt]['daily_spent'] = $total_spent;
                        //$spend[$dt]['daily_budget'] = $daily_target;
                        $spend[$dt]['daily_percent_spent'] = $spend_per;
                        $spend[$dt]['left_to_spend'] = $working_budget - $so_far;
                        $spend[$dt]['percent_spent_to_date'] =0;
                        $pstd=$data['daily_budget'] * $current_day;
                        if($pstd>0){
                            $pstd=$so_far / $pstd;
                            $spend[$dt]['percent_spent_to_date'] = $pstd * 100;
                        }
                        if(($time_data['time_fm']['days'] - $current_day) != 0 ) {
                            //$spend[$dt]['percent_100'] = ($working_budget - $so_far) / ($time_data['time_fm']['days'] - $current_day);
                        } else {
                            //$spend[$dt]['percent_100'] = '';
                        }
                        $current_day++;
                    }
                    $i = 1;
                    $mtd_spend = 0;
                    foreach($period as $p){

                        $d =  $p->format('m/d/Y');

                        if(isset($spend[$d])) {
                            $spend[$d]['day'] = $i;

                            if($i == 1){
                                $target_spend = $working_budget/$time_data['time_fm']['days'];
                            } else {
                                $target_spend = ($working_budget - $mtd_spend) / ($time_data['time_fm']['days'] - $i+1);
                            }

                            $mtd_spend += $spend[$d]['daily_spent'];

                            if(($time_data['time_fm']['days'] - $i) != 0 ) {
                                $percent_100 = ($working_budget - $mtd_spend) / ($time_data['time_fm']['days'] - $i);
                            } else {
                                $percent_100 = '';
                            }

                            $spend[$d]['should_be'] = ($working_budget/$time_data['time_fm']['days']) * $i;
                            $spend[$d]['percent_spent_to_date']=0;
                            $pstd=($data['daily_budget'] * $i);
                            if($pstd>0){
                                $spend[$d]['percent_spent_to_date'] = ($mtd_spend / $pstd) * 100;
                            }
                            $spend[$d]['daily_budget'] = $target_spend;
                            $spend[$d]['left_to_spend'] = $working_budget - $mtd_spend;
                            $spend[$d]['percent_100'] = $percent_100;

                            $data['spend'][] = $spend[$d];
                        } else {
                            if(($time_data['time_fm']['days'] - $i) != 0 ) {
                                $percent_100 = ($working_budget - $mtd_spend) / ($time_data['time_fm']['days'] - $i);
                            } else {
                                $percent_100 = '';
                            }
                            if($i == 1){
                                $target_spend = $working_budget/$time_data['time_fm']['days'];
                            } else {
                                $target_spend = ($working_budget - $mtd_spend) / ($time_data['time_fm']['days'] - $i+1);
                            }
                            $pstd=($data['daily_budget'] * $i);
                            if($pstd>0){
                                $pstd = ($mtd_spend / $pstd) * 100;
                            }
                            $data['spend'][] = [
                                'id' => '0',
                                'date' => $d,
                                'day' => $i,
                                'google' => 0,
                                'bing' => 0,
                                'facebook' => 0,
                                'so_far' => $mtd_spend,
                                'should_be' => ($working_budget/$time_data['time_fm']['days']) * $i,
                                'daily_spent' => 0,
                                'daily_budget' => $target_spend,
                                'daily_percent_spent' => 0,
                                'left_to_spend' => $working_budget - $mtd_spend,
                                'percent_spent_to_date' => $pstd,
                                'percent_100' => $percent_100
                            ];
                        }
                        $i++;
                    }
                }
            } else {
                echo json_encode(array('success' => '0', 'msg'=> 'There is an error with the platform input', 'cid' => $fields));
                exit;
            }
            
        }
        echo json_encode($data);
    }

    /*
     * Get_Daily_DOM_Spend
     * Used as an ajax endpoint to get spending for all SEM clients.
     *
     * @input month - Month of data we want the spend data for
     * @input year - Year we want the spend data for
     *
     * @author KA <kaustin@dom360.com>
     * @version 2016-04-01
     */
    public function get_dom_monthly_sem(){

        //get post variables and validate input
        $fields = $this->input->post();
        $result = 1;
        $r=[];
        (isset($fields['month'])) ? $month = $fields['month'] : $month = date("m");
        (isset($fields['year'])) ? $year = $fields['year'] : $year = date("Y");

        if(($result == 0) || (checkdate($month,'01',$year) === false) || ($this->atDOMLevel != true))
        {
            echo json_encode(array('success' => '0', 'msg'=> 'There is an error with the client supplied'));
            exit;
        }
        else
        {
            //initialize variables
            $data = array();

            $this->load->model(array('domalerts', 'domsembudget'));
            $clients = $this->domalerts->get_sem_clients('56',null);
            $time_data = $this->domsem->get_budget_dates($year,$month); //get exact dates/time/days in the time format needed
            $teams = $this->domclients->get_client_teams();
            $team_name = '';
            $i=0;
            $now = time();
            $first_of_month =  strtotime('m/01/Y');
            $last_of_month = strtotime('last day of this month');
            $end_date = $time_data['time_fm']['end_date'];
            $begin_date = $time_data['time_fm']['begin_date'];
            if($end_date < $now){
                $elapsed_days = $time_data['time_fm']['days'];
            }else if($last_of_month < $begin_date){
                $elapsed_days = 0;
            } else{
                $elapsed_days = date('d') -1;
            }
            $summary = [
                'rollover' => 0,
                'amount_received' => 0,
                'net_budget' => 0,
                'mtd_target'    => 0,
                'actual_spend' => 0,
                'mtd_variance' => 0
                ];


            foreach ($clients as $client) {
                $working_budget = $admin_fee_amount = $so_far = $tot_rollover = $tot_fee = $tot_pay = 0;
                $current_day = 1;
                $budget = $this->domsem->get_monthly_sem_budget($client['client_id'],$time_data['time_fm']['begin_date'], $time_data['time_fm']['end_date']);
                $note = $this->domsem->get_client_spend_notes($client['client_id'], $month, $year);
                $team = $this->domclients->get_client_team($client['client_id']);
                foreach ($teams as $t){
                    if($t['id']=== $team){
                        $team_name = $t['name'];
                    }
                }
                $team_class = $this->domclients->get_client_team_class_name($client['client_id']);

                $data[$i]['client_id'] = $client['client_id'];
                $data[$i]['client_name'] = $client['client_name'];
                $data[$i]['client_code'] = $client['client_code'];
                $data[$i]['client_team_class'] = $team_class;
                $data[$i]['client_team'] = $team_name;
                $data[$i]['begin_date'] = $time_data['date_fm']['begin_date'];
                $data[$i]['end_date'] = $time_data['date_fm']['end_date'];
                $data[$i]['total_days'] = $time_data['time_fm']['days']; //total days in the monthtripped from
                $data[$i]['note'] = $note;
                $data[$i]['budget_id'] = $budget['budget_id'];
                //Getting budget with budget payments.
                $rollovers = $this->domsembudget->get_rollover($client['client_id'], $month, $year);
                $fees = $this->domsembudget->get_fees($client['client_id'], $month, $year);
                $payments = $this->domsembudget->get_payments($client['client_id'], $month, $year);
                $tot_fee = $tot_rollover = $tot_pay = 0;
                foreach ($rollovers as $r){
                    if($r['trans_type'] == 2){
                        $tot_rollover += $r['amount'];
                    } else if($r['trans_type'] == 4){
                        $tot_rollover -= $r['amount'];
                    }
                }
                foreach ($fees as $f){
                    $tot_fee += $f['amount'];
                }
                foreach ($payments as $p) {
                    $tot_pay += $p['amount'];
                }
                $data[$i]['total_budget'] = $tot_pay;
                $data[$i]['Account'] = number_format(($tot_pay), 2, '.', '');
                $data[$i]['Roll'] = $tot_rollover;
                $data[$i]['admin_fee_amount'] = $tot_fee;
                $working_budget = $tot_pay - $tot_fee + $tot_rollover;
                $data[$i]['Budget'] = $working_budget;
                if ($budget['admin_type'] == '1') {
                    $data[$i]['Admin'] = floatval($budget['admin_fee']) . '%';
                } elseif ($budget['admin_type'] == '2') {
                    $data[$i]['Admin'] = '$' . $budget['admin_fee'];
                } elseif ($budget['admin_type'] == '0') {
                    $data[$i]['Admin'] = '$0.00';
                } else {
                    $data[$i]['Admin'] = 'n/a';
                }

                $daily = $this->domsem->get_client_spend_data($client['client_id'],$time_data['date_fm']['begin_date'], $time_data['date_fm']['end_date']);
                $data[$i]['Daily'] = $daily;

                foreach ($daily as $d) {
                    $total_spent = $spend_per = $left_to_spend = 0;
                    if(is_numeric($d['google_spend'])) { $total_spent += $d['google_spend']; }
                    if(is_numeric($d['bing_spend'])) { $total_spent += $d['bing_spend']; }
                    if(is_numeric($d['fb_spend'])) { $total_spent += $d['fb_spend']; }
                    $so_far += $total_spent;
                    $current_day++;
                }
                //Calculate latest day's' spend.
                $latest_spend = $daily[$elapsed_days - 1]['google_spend'] + $daily[$elapsed_days - 1]['bing_spend'] + $daily[$elapsed_days - 1]['fb_spend'];
                $data[$i]['Days'] = $elapsed_days;
                $data[$i]['latest_spend'] = $latest_spend;
                $data[$i]['Should'] = number_format(((($working_budget /$time_data['time_fm']['days']) * $elapsed_days)) - $latest_spend , 2, '.', '');
                $data[$i]['Spent'] = $so_far;
                $data[$i]['Percent']=0;
                $percent=0;
                if($time_data['time_fm']['days']>0){
                    $p1=($working_budget / $time_data['time_fm']['days']) * $elapsed_days;
                    $percent=number_format($p1, 2, '.', '');
                }
                if($percent>0) $data[$i]['Percent'] = ($so_far/$percent) * 100;
                if(($time_data['time_fm']['days'] - $elapsed_days) != 0 ) {
                    $data[$i]['Target'] = ($working_budget - $so_far) / ($time_data['time_fm']['days'] - $elapsed_days);
                } else {
                    $data[$i]['Target'] = 0;
                }
                $summary['rollover'] += $tot_rollover;
                $summary['amount_received'] += $tot_pay;
                $summary['net_budget'] += $working_budget;
                $summary['actual_spend'] += $so_far;
                $summary['mtd_target'] += ((($working_budget /$time_data['time_fm']['days']) * $elapsed_days));
                $i++;
            
            }
            $d=($summary['net_budget']/$time_data['time_fm']['days']);
            $d*=$elapsed_days;
            if($d>0) $summary['mtd_variance'] = $summary['actual_spend']/$d;
            $r = [
                'clients' => $data,
                'summary' => $summary
            ];
        }
        echo json_encode($r);
    }


    /*
     * Get_DOM_DAILY_BING
     * Used as an ajax endpoint to get spending for all SEM clients.
     *
     * @input month - Month of data we want the spend data for
     * @input year - Year we want the spend data for
     *
     * @author KA <kaustin@dom360.com>
     * @version 2016-04-01
     */
    public function get_dom_daily_bing(){

        //get post variables and validate input
        $fields = $this->input->post();
        $result = 1;

        (isset($fields['month'])) ? $month = $fields['month'] : $month = date("m");
        (isset($fields['year'])) ? $year = $fields['year'] : $year = date("Y");

        if(($result == 0) || (checkdate($month,'01',$year) === false) || ($this->atDOMLevel != true))
        {
            echo json_encode(array('success' => '0', 'msg'=> 'There is an error with the input data'));
            exit;
        }
        else
        {
            //initialize variables
            $data = array();

            $this->load->model('domalerts');
            $clients = $this->domalerts->get_sem_clients('56',null);
            $time_data = $this->domsem->get_budget_dates($year,$month); //get exact dates/time/days in the time format needed
            $teams = $this->domclients->get_client_teams();
            $team_name = '';
            $i=0;

            foreach ($clients as $client) {
                $working_budget = $admin_fee_amount = $so_far = 0;
                $current_day = 1;
                $budget = $this->domsem->get_monthly_sem_budget($client['client_id'],$time_data['time_fm']['begin_date'], $time_data['time_fm']['end_date']);
                $team = $this->domclients->get_client_team($client['client_id']);
                $team = $this->domclients->get_client_team($client['client_id']);
                foreach ($teams as $t){
                    if($t['id']=== $team){
                        $team_name = $t['name'];
                    }
                }
                $team_class = $this->domclients->get_client_team_class_name($client['client_id']);


                $data[$i]['client_id'] = $client['client_id'];
                $data[$i]['client_name'] = $client['client_name'];
                $data[$i]['client_code'] = $client['client_code'];
                $data[$i]['client_team_class'] = $team_class;
                $data[$i]['client_team'] = $team_name;
                $data[$i]['begin_date'] = $time_data['date_fm']['begin_date'];
                $data[$i]['end_date'] = $time_data['date_fm']['end_date'];
                $data[$i]['total_days'] = $time_data['time_fm']['days']; //total days in the month
                $data[$i]['row_total'] = 0;
                $daily = $this->domsem->get_client_spend_data($client['client_id'],$time_data['date_fm']['begin_date'], $time_data['date_fm']['end_date']);
                $j=0;
                $j=0;
                $end_date = new DateTime($time_data['date_fm']['end_date']);
                $end_date->modify('+1 day');
                $period = new DatePeriod(
                    new DateTime($time_data['date_fm']['begin_date']),
                    new DateInterval('P1D'),
                    $end_date
                );
                $spend = [];
                foreach ($daily as $d)
                {
                    $spend[ date('m/d/Y', strtotime($d['date']))] = [
                        'id' => $d['id'],
                        'date' => date('m/d/Y', strtotime($d['date'])),
                        'day' => $current_day,
                        'bing' => $d['bing_spend']
                    ];
                    $data[$i]['row_total'] += $d['bing_spend'];
                    $current_day++;
                    $j++;
                }
                $k = 1;
                foreach($period as $p){
                    $d =  $p->format('m/d/Y');
                    if(is_array($spend[$d])) {
                        $spend[$d]['day'] = $k;
                        $data[$i]['spend'][] = $spend[$d];
                    } else {

                        $data[$i]['spend'][] = [
                            'id' => '0',
                            'date' => $d,
                            'day' => $k,
                            'bing' => 0,

                        ];
                    }
                    $k++;
                }
                $i++;
            }

            echo json_encode($data);

        }
    }


    /*
     * Get_DOM_DAILY_GOOGLE
     * Used as an ajax endpoint to get spending for all SEM clients.
     *
     * @input month - Month of data we want the spend data for
     * @input year - Year we want the spend data for
     *
     * @author OK <kasutin@dom360.com>
     * @version 2016-04-01
     */
    public function get_dom_daily_google(){

        //get post variables and validate input
        $fields = $this->input->post();
        $result = 1;

        (isset($fields['month'])) ? $month = $fields['month'] : $month = date("m");
        (isset($fields['year'])) ? $year = $fields['year'] : $year = date("Y");

        if(($result == 0) || (checkdate($month,'01',$year) === false) || ($this->atDOMLevel != true))
        {
            echo json_encode(array('success' => '0', 'msg'=> 'There is an error with the input data'));
            exit;
        }
        else
        {
            //initialize variables
            $data = array();

            $this->load->model('domalerts');
            $clients = $this->domalerts->get_sem_clients('56',null);
            $time_data = $this->domsem->get_budget_dates($year,$month); //get exact dates/time/days in the time format needed
            $teams = $this->domclients->get_client_teams();
            $team_name = '';
            $i=0;

            foreach ($clients as $client) {
                $working_budget = $admin_fee_amount = $so_far = 0;
                $current_day = 1;
                $budget = $this->domsem->get_monthly_sem_budget($client['client_id'],$time_data['time_fm']['begin_date'], $time_data['time_fm']['end_date']);
                $team = $this->domclients->get_client_team($client['client_id']);
                $team = $this->domclients->get_client_team($client['client_id']);
                foreach ($teams as $t){
                    if($t['id']=== $team){
                        $team_name = $t['name'];
                    }
                }
                $team_class = $this->domclients->get_client_team_class_name($client['client_id']);

                $data[$i]['client_id'] = $client['client_id'];
                $data[$i]['client_name'] = $client['client_name'];
                $data[$i]['client_code'] = $client['client_code'];
                $data[$i]['client_team_class'] = $team_class;
                $data[$i]['client_team'] = $team_name;
                $data[$i]['begin_date'] = $time_data['date_fm']['begin_date'];
                $data[$i]['end_date'] = $time_data['date_fm']['end_date'];
                $data[$i]['total_days'] = $time_data['time_fm']['days']; //total days in the month
                $data[$i]['row_total'] = 0;
                $daily = $this->domsem->get_client_spend_data($client['client_id'],$time_data['date_fm']['begin_date'], $time_data['date_fm']['end_date']);
                $j=0;
                $end_date = new DateTime($time_data['date_fm']['end_date']);
                $end_date->modify('+1 day');
                $period = new DatePeriod(
                    new DateTime($time_data['date_fm']['begin_date']),
                    new DateInterval('P1D'),
                    $end_date
                );
                $spend = [];
                foreach ($daily as $d)
                {
                    $spend[ date('m/d/Y', strtotime($d['date']))] = [
                        'id' => $d['id'],
                        'date' => date('m/d/Y', strtotime($d['date'])),
                        'day' => $current_day,
                        'google' => $d['google_spend']
                    ];
                    $data[$i]['row_total'] += $d['google_spend'];
                    $current_day++;
                    $j++;
                }
                $k = 1;
                foreach($period as $p){
                    $d =  $p->format('m/d/Y');
                    if(is_array($spend[$d])) {
                        $spend[$d]['day'] = $k;
                        $data[$i]['spend'][] = $spend[$d];
                    } else {

                        $data[$i]['spend'][] = [
                            'id' => '0',
                            'date' => $d,
                            'day' => $k,
                            'google' => 0,

                        ];
                    }
                    $k++;
                }
                $i++;
            }

            echo json_encode($data);

        }
    }


    /*
     * Get_DOM_DAILY_FACEBOOK
     * Used as an ajax endpoint to get spending for all SEM clients.
     *
     * @input month - Month of data we want the spend data for
     * @input year - Year we want the spend data for
     *
     * @author KA <kaustin@dom360.com>
     * @version 2016-04-01
     */
    public function get_dom_daily_facebook(){

        //get post variables and validate input
        $fields = $this->input->post();
        $result = 1;

        (isset($fields['month'])) ? $month = $fields['month'] : $month = date("m");
        (isset($fields['year'])) ? $year = $fields['year'] : $year = date("Y");

        if(($result == 0) || (checkdate($month,'01',$year) === false) || ($this->atDOMLevel != true))
        {
            echo json_encode(array('success' => '0', 'msg'=> 'There is an error with the input data'));
            exit;
        }
        else
        {
            //initialize variables
            $data = array();

            $this->load->model('domalerts');
            $clients = $this->domalerts->get_sem_clients('56',null);
            $time_data = $this->domsem->get_budget_dates($year,$month); //get exact dates/time/days in the time format needed

            $teams = $this->domclients->get_client_teams();
            $team_name = '';
            $i=0;

            foreach ($clients as $client) {
                $working_budget = $admin_fee_amount = $so_far = 0;
                $current_day = 1;
                $budget = $this->domsem->get_monthly_sem_budget($client['client_id'],$time_data['time_fm']['begin_date'], $time_data['time_fm']['end_date']);
                $team = $this->domclients->get_client_team($client['client_id']);
                foreach ($teams as $t){
                    if($t['id']=== $team){
                        $team_name = $t['name'];
                    }
                }
                $team_class = $this->domclients->get_client_team_class_name($client['client_id']);

                $data[$i]['client_id'] = $client['client_id'];
                $data[$i]['client_name'] = $client['client_name'];
                $data[$i]['client_code'] = $client['client_code'];
                $data[$i]['client_team_class'] = $team_class;
                $data[$i]['client_team'] = $team_name;
                $data[$i]['begin_date'] = $time_data['date_fm']['begin_date'];
                $data[$i]['end_date'] = $time_data['date_fm']['end_date'];
                $data[$i]['total_days'] = $time_data['time_fm']['days']; //total days in the month
                $data[$i]['row_total'] = 0;
                $daily = $this->domsem->get_client_spend_data($client['client_id'],$time_data['date_fm']['begin_date'], $time_data['date_fm']['end_date']);
                $j=0;
                $end_date = new DateTime($time_data['date_fm']['end_date']);
                $end_date->modify('+1 day');
                $period = new DatePeriod(
                    new DateTime($time_data['date_fm']['begin_date']),
                    new DateInterval('P1D'),
                    $end_date
                );
                $spend = [];
                foreach ($daily as $d)
                {
                    $spend[ date('m/d/Y', strtotime($d['date']))] = [
                        'id' => $d['id'],
                        'date' => date('m/d/Y', strtotime($d['date'])),
                        'day' => $current_day,
                        'facebook' => $d['fb_spend']
                    ];
                    $current_day++;
                    $j++;
                    $data[$i]['row_total'] += $d['fb_spend'];
                }
                $k = 1;
                foreach($period as $p){
                    $d =  $p->format('m/d/Y');
                    if(is_array($spend[$d])) {
                        $spend[$d]['day'] = $k;
                        $data[$i]['spend'][] = $spend[$d];
                    } else {

                        $data[$i]['spend'][] = [
                            'id' => '0',
                            'date' => $d,
                            'day' => $k,
                            'facebook' => 0,

                        ];
                    }
                    $k++;

                }
                $i++;
            }

            echo json_encode($data);

        }
    }
    /**
     * SET_FACEBOOK_SPEND
     * Used as an ajax endpoint to set facebook Spending for one day.
     *
     * @input client_id - Client id of the client we want the Daily FB spend data for
     * @input row_id - ID passed in from get function representing the row in the DB that needs to be updated.
     * @input amount - Amount of Money Spent on Facebook for that day. Float expected.
     *
     * @author KA <kaustin@dom360.com>
     * @version 2016-04-01
     */
    public function set_facebook_spend()
    {
        $fields = $this->input->post();
        $r = 0;
        $rows = array();
        if(($fields['client_id'] != $this->client_id) && $this->atDOMLevel != true)
        {
            echo json_encode(array('success' => '0', 'msg'=> 'There is an error with the input data', 'cid'=> $fields['client_id']));
            exit;
        }
        else {
            foreach($fields['days'] as $day){
                if($day['row_id'] && $this->domsem->update_client_sem_spend($day['row_id'], array('fb_spend'=>$day['amount']))){
                    $r = 1;
                } elseif ($this->domsem->insert_daily_client_sem_spend(['client_id' => $fields['client_id'], 'date' => $day['date'], 'fb_spend' => $day['amount']])) {
                    $r = 1;
                }
                $rows[] = $day['amount'];
            }

            echo json_encode(array('success' => '1', 'msg'=> 'Facebook Spend Amount Was Successfully Updated', 'post' => $rows));
            exit;
        }

    }
    /*
     * GET_FACEBOOK_SPEND
     * Used as an ajax endpoint to get spending for all SEM clients.
     *
     * @input client_id - Client id of the client we want the Daily FB spend data for
     * @input month - Month we want the spend data for
     * @input day - Day we want the spend data for
     * @input year - Year we want the spend data for
     *
     * @author KA <kaustin@dom360.com>
     * @version 2016-04-01
     */
    public function get_facebook_spend()
    {
        //get post variables and validate input
        $fields = $this->input->post();
        $result = 1;

        (isset($fields['client_id'])) ? $client_id = $fields['client_id'] : $result = 0;
        (isset($fields['month'])) ? $month = $fields['month'] : $result = 0;
        (isset($fields['day'])) ? $day = $fields['day'] : $result = 0;
        (isset($fields['year'])) ? $year = $fields['year'] : $result = 0;

        if(($result == 0) || (checkdate($month,$day,$year) === false) || ($client_id != $this->client_id))
        {
            echo json_encode(array('success' => '0', 'msg'=> 'There is an error with the input data', 'cid'=> $fields['client_id']));
            exit;
        }
        else
        {
            $date = date("Y-m-d", strtotime($month.'/'.$day.'/'.$year));
            if($daily = $this->domsem->get_client_spend_data($client_id,$date,$date))
            {
                foreach ($daily as $d)
                {
                    $data['id'] = $d['id'];
                    $data['date'] = date('m/d/Y', strtotime($d['date']));
                    (is_numeric($d['fb_spend'])) ? $data['amount'] = $d['fb_spend']: $data['amount'] = '0.00';
                }

                echo json_encode($data);
            }
            else
            {
                echo json_encode(array('success' => '0', 'msg'=> 'There is an error with retrieving FB Spend Data', 'cid'=> $fields['client_id']));
                exit;
            }
        }

    }

    /*
     * GET_BING_SPEND
     * Used as an ajax endpoint to get spending for all SEM clients.
     *
     * @input client_id - Client id of the client we want the Daily Bing spend data for
     * @input month - Month we want the spend data for
     * @input day - Day we want the spend data for
     * @input year - Year we want the spend data for
     *
     * @author KA <kaustin@dom360.com>
     * @version 2016-04-01
     */
    public function get_bing_spend()
    {
        //get post variables and validate input
        $fields = $this->input->post();
        $result = 1;

        (isset($fields['client_id'])) ? $client_id = $fields['client_id'] : $result = 0;
        (isset($fields['month'])) ? $month = $fields['month'] : $result = 0;
        (isset($fields['day'])) ? $day = $fields['day'] : $result = 0;
        (isset($fields['year'])) ? $year = $fields['year'] : $result = 0;

        if(($result == 0) || (checkdate($month,$day,$year) === false) || ($client_id != $this->client_id))
        {
            echo json_encode(array('success' => '0', 'msg'=> 'There is an error with the input data', 'cid'=> $fields['client_id']));
            exit;
        }
        else
        {
            $date = date("Y-m-d", strtotime($month.'/'.$day.'/'.$year));
            if($daily = $this->domsem->get_client_spend_data($client_id,$date,$date))
            {
                foreach ($daily as $d)
                {
                    $data['id'] = $d['id'];
                    $data['date'] = date('m/d/Y', strtotime($d['date']));
                    (is_numeric($d['bing_spend'])) ? $data['amount'] = $d['bing_spend']: $data['amount'] = '0.00';
                }

                echo json_encode($data);
            }
            else
            {
                echo json_encode(array('success' => '0', 'msg'=> 'There is an error with retrieving Bing Spend Data', 'cid'=> $fields['client_id']));
                exit;
            }
        }

    }


    /*
         * GET_GOOGLE_SPEND
         * Used as an ajax endpoint to get spending for all SEM clients.
         *
         * @input client_id - Client id of the client we want the Daily Google spend data for
         * @input month - Month we want the spend data for
         * @input day - Day we want the spend data for
         * @input year - Year we want the spend data for
         *
         * @author KA <kaustin@dom360.com>
         * @version 2016-04-01
         */
    public function get_google_spend()
    {
        //get post variables and validate input
        $fields = $this->input->post();
        $result = 1;

        (isset($fields['client_id'])) ? $client_id = $fields['client_id'] : $result = 0;
        (isset($fields['month'])) ? $month = $fields['month'] : $result = 0;
        (isset($fields['day'])) ? $day = $fields['day'] : $result = 0;
        (isset($fields['year'])) ? $year = $fields['year'] : $result = 0;

        if(($result == 0) || (checkdate($month,$day,$year) === false) || ($client_id != $this->client_id))
        {
            echo json_encode(array('success' => '0', 'msg'=> 'There is an error with the input data', 'cid'=> $fields['client_id']));
            exit;
        }
        else
        {
            $date = date("Y-m-d", strtotime($month.'/'.$day.'/'.$year));
            if($daily = $this->domsem->get_client_spend_data($client_id,$date,$date))
            {
                foreach ($daily as $d)
                {
                    $data['id'] = $d['id'];
                    $data['date'] = date('m/d/Y', strtotime($d['date']));
                    (is_numeric($d['google_spend'])) ? $data['amount'] = $d['google_spend']: $data['amount'] = '0.00';
                }

                echo json_encode($data);
            }
            else
            {
                echo json_encode(array('success' => '0', 'msg'=> 'There is an error with retrieving Google Spend Data', 'cid'=> $fields['client_id']));
                exit;
            }
        }

    }

    /*
     * GET_VIDEO_SPEND
     * Used as an ajax endpoint to get spending for all SEM clients.
     *
     * @input client_id - Client id of the client we want the Daily Google spend data for
     * @input month - Month we want the spend data for
     * @input day - Day we want the spend data for
     * @input year - Year we want the spend data for
     *
     * @return [array]
     *
     * @author KA <kaustin@dom360.com>
     * @version 2016-04-01
     */
    public function get_dom_daily_video()
    {

        /*ini_set('max_execution_time',300);
        set_time_limit(90);
        //get post variables and validate input
        $fields = $this->input->post();
        $result = 1;
        (isset($fields['month'])) ? $month = $fields['month'] : $month = date("m");
        (isset($fields['year'])) ? $year = $fields['year'] : $year = date("Y");

        $data = [];
        if(($result == 0) || (checkdate($month,'05',$year) === false) || ($this->atDOMLevel != true))
        {
            echo json_encode(array('success' => '0', 'msg'=> 'There is an error with the input data'));
            exit;
        }
        else
        {
            $clients = $this->domalerts->get_sem_clients('56',null);
            $time_data = $this->domsem->get_budget_dates($year,$month); //get exact dates/time/days in the time format needed
            $from = date('Y-m-d', strtotime($time_data['date_fm']['begin_date']));
            $to = date('Y-m-d', strtotime($time_data['date_fm']['end_date']));
            $interval =  DateInterval::createFromDateString('1 day');
            $from_date = new DateTime($from);
            $to_date = new DateTime($to);
            $to_date->modify('+1 day');
            $period = new DatePeriod($from_date, $interval, $to_date);
            $teams = $this->domclients->get_client_teams();
            $team_name = '';
            $dates = [];
            foreach($period as $dt){
                $day = $dt->format( "Y-m-d" );
                $dates[$day] = 0;
            }

            foreach($clients as $client){
                $team = $this->domclients->get_client_team($client['client_id']);
                foreach ($teams as $t){
                    if($t['id']=== $team){
                        $team_name = $t['name'];
                    }
                }
                $team_class = $this->domclients->get_client_team_class_name($client['client_id']);
                $modules = $this->domclients->get_client_modules($client['client_id']);
                if(array_key_exists(70, $modules)) {
                    $data[$client['client_code']] = array(
                        'client_id' => $client['client_id'],
                        'client_code' => $client['client_code'],
                        'client_name' => $client['client_name'],
                        'client_team_class' => $team_class,
                        'client_team' => $team_name,
                        'ads_id' => $client[8],
                        'days' => $time_data['time_fm']['days'],
                        'spend' => $dates
                    );
                    try{
                        $spend = $this->domsem->adwords_video_daily($client[8], $time_data['time_fm']['begin_date'], $time_data['time_fm']['end_date']);
                        foreach ($spend as $s) {
                            if (isset($s['video_date'])) {
                                $data[$client['client_code']]['spend'][$s['video_date']] += $s['video_cost'];
                            } else {
                                $data[$client['client_code']]['spend'][$s['video_date']] = $s['video_cost'];
                            }
                        }
                    }catch(Exception $e){
                        continue;
                    }
                }
            }
            if($data){
                echo json_encode($data);
            } else {
                echo json_encode(array('success' => '0', 'msg'=> 'There is an error with retrieving Video Spend Data'));
                exit;
            }
        }*/
        echo json_encode(['result'=>0]);
    }

    /*
     * GET_CLIENT_NOTE
     * Used as an ajax endpoint to get client note.
     *
     * @input client_id - Client id of the client we want the Daily Google spend data for
     * @input month - Month we want the note data for
     * @input day - Day we want the note data for
     * @input year - Year we want the note data for
     *
     * @return [array]
     *
     * @author KA <kaustin@dom360.com>
     * @version 2016-04-01
     */
    public function get_client_note(){
        $cid = $this->input->get('cid', true);
        $month = $this->input->get('month', true);
        $year = $this->input->get('year', true);

        $data = $this->domsem->get_client_spend_notes($cid, $month, $year);

        echo json_encode($data);
    }

    /*
     * Add_CLIENT_NOTE
     * Add Client Note.
     *
     * @input client_id - Client id of the client we want to save the note to.
     * @input month - Month we want  to save the note to.
     * @input year - Year we want to save the note to.
     *
     * @return [array]
     *
     * @author KA <kaustin@dom360.com>
     * @version 2016-04-01
     */
    public function add_client_note(){
        $fields = $this->input->post();
        $data = array(
            'cid' => $fields['cid'],
            'month' => $fields['month'],
            'year' => $fields['year'],
            'uid' => $this->user['UserID'],
            'note' => $fields['note']
        );
            $r = $this->domsem->add_client_spend_note($data);

        if($r === TRUE){
            echo json_encode(array('success' => 1, 'message' => 'Note successfully updated.'));
        } else {
            echo json_encode(array('success' => 0, 'message' => 'An error occurred while saving the data.'));
        }
    }

    public function update_client_note(){
        $fields = $this->input->post();
        $data = array(
            'cid' => $fields['cid'],
            'month' => $fields['month'],
            'year' => $fields['year'],
            'uid' => $this->user['UserID'],
            'note' => $fields['note']
        );
        $r = $this->domsem->update_client_spend_note($fields['id'], $data);

        if($r === TRUE){
            echo json_encode(array('success' => 1, 'message' => 'Note successfully updated.'));
        } else {
            echo json_encode(array('success' => 0, 'message' => 'An error occurred while saving the data.'));
        }
    }
}