<?=page_loader();?>
<div class="page-content">
    <?php notifyError(); ?>
    <div class="page-title">
        <h5>SEM Campaign Manager: <span><?= get_client_name();?></span></h5>
        <div class="pull-right form-inline">
            <select name="timeFrame" class="select-liquid" id="month" placeholder="Select Time Frame">
                <?php
                $month_options = '';
                $curr_month = date('m');
                for( $i = 1; $i <= 12; $i++ ) {
                    $month_num = str_pad( $i, 2, 0, STR_PAD_LEFT );
                    $year=date('Y');
                    $month_name = date( 'M',strtotime($year."/".$i."/25"));
                    $month_options .= '<option '.($curr_month == $month_num ? 'selected="selected"' : '').' value="' . $month_num . '">' . $month_name . '</option>';
                }
                echo $month_options;
                ?>
            </select>
            <select class="select-liquid" id="year">
                <?= '<option selected="selected" value="'.date('Y').'">'.date('Y').'</option><option value="'.date("Y",strtotime("-1 year")).'">'.date("Y",strtotime("-1 year")).'</option>'; ?>
            </select>
            <button type="button" id="apply_date" class="btn btn-info">Apply</button>
            <button id="exportPDF" class="btn btn-primary pull-right" title="Export to PDF" onclick="export_pdf()"><i class="fa fa-file-pdf-o"></i> PDF</button>
        </div>
    </div>
    <div id="client_notes">

    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h6 class="panel-title">Daily Data</h6>
        </div>
        <div class="table">
            <?=ajax_loader()?>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?=base_url()?>js/ad_manager.js"></script>
<script type="text/javascript">
    var month = $('#month').val();
    var year = $('#year').val();
    $('#apply_date').on('click', function(){
        var month = $('#month').val();
        var year = $('#year').val();
        get_page_client_sem(cid, month, year);
    });

    get_page_client_sem(cid, month, year);
    function get_page_client_sem(cid, month, year){
        if ( $.fn.dataTable.isDataTable( '#vc-table' ) ) {
            var table = $('#vc-table').dataTable();
            table.fnDestroy();
        }
        $('#vc-table').find('tbody').html('<tr><td colspan="11">'+ajax_loader+'</td></tr>');
        $('.table').html(ajax_loader);
        var form = document.createElement('form');
        var client = document.createElement('input');
        client.setAttribute('type', 'hidden');
        client.setAttribute('name', 'client_id');
        client.setAttribute('value', cid);
        form.appendChild(client);
        var monthData = document.createElement('input');
        monthData.setAttribute('type', 'hidden');
        monthData.setAttribute('name', 'month');
        monthData.setAttribute('value', month);
        form.appendChild(monthData);
        var yearData = document.createElement('input');
        yearData.setAttribute('type', 'hidden');
        yearData.setAttribute('name', 'year');
        yearData.setAttribute('value', year);
        form.appendChild(yearData);
        var formData = $(form).serialize();
        $.ajax({
            url: '/advertising/manage/get_daily_client_spend',
            method: 'POST',
            data: formData,
            dataType: 'json',
            success: function(data){
                if(data.spend != undefined) {
                    var table_html = '<table id="vc-table" class=" table table-striped table-bordered">'+
                        '<thead>'+
                            '<tr>'+
                                '<th scope="col">Date</th>'+
                                '<th scope="col">Day</th>'+
                                '<th scope="col">Daily Target Spend</th>'+
                                '<th scope="col">Google Spend for Day</th>'+
                                '<th scope="col">Bing Spend for Day</th>'+
                                '<th scope="col">Facebook Spend for Day</th>'+
                                '<th scope="col">Total Spend for Day</th>'+
                                '<th scope="col">Actual Spend for Month</th>'+
                                '<th scope="col">MTD Target Spend</th>'+
                                '<th scope="col">MTD Variance</th>'+
                                '<th scope="col">Next Day Target Spend</th>'+
                            '</tr>'+
                        '</thead>'+
                        '<tbody>'+
                        '</tbody>'+
                    '</table>';
                    $('.table').html(table_html).removeClass('has-padding');
                    var html = '';
                    for (var i = 0; i < data.spend.length; i++) {
                        var spend = data.spend[i];
                        if (spend.facebook === null) {
                            spend.facebook = 0;
                        }
                        if (!spend.percent_98) {
                            spend.percent_98 = 0;
                        }
                        if (!spend.percent_99) {
                            spend.percent_99 = 0;
                        }
                        if (!spend.percent_100) {
                            spend.percent_100 = 0;
                        }
                        html += '<tr id="' + spend.id + '">' +
                            '<td scope="row">' + spend.date + '</td>' +
                            '<td>' + spend.day + '</td>' +
                            '<td class="spend">' + Number(spend.daily_budget).toLocaleString('en', {style: "currency", currency: "USD"}) + '</td>' +
                            '<td class="google">' + Number(spend.google).toLocaleString('en', {style: "currency", currency: "USD"}) + '</td>' +
                            '<td class="bing">' + Number(spend.bing).toLocaleString('en', {style: "currency", currency: "USD"}) + '</td>' +
                            '<td class="facebook">' + Number(spend.facebook).toLocaleString('en', {style: "currency", currency: "USD"}) + '</td>' +
                            '<td class="total">' + spend.daily_spent.toLocaleString('en', {style: "currency", currency: "USD"}) + '</td>' +
                            '<td class="should">' + spend.so_far.toLocaleString('en', {style: "currency", currency: "USD"}) + '</td>' +
                            '<td class="so-far">' + spend.should_be.toLocaleString('en', {style: "currency", currency: "USD"}) + '</td>' +
                            '<td class="percent">' + Number(Math.round((spend.percent_spent_to_date) + 'e2') + 'e-2') + '%</td>' +
                            //'<td>' + Number(Math.round(spend.percent_98 + 'e2') + 'e-2').toLocaleString('en', {style: "currency", currency: "USD"}) + '</td>' +
                            //'<td>' + Number(Math.round(spend.percent_99 + 'e2') + 'e-2').toLocaleString('en', {style: "currency", currency: "USD"}) + '</td>' +
                            '<td class="percent-100">' + Number(Math.round(spend.percent_100 + 'e2') + 'e-2').toLocaleString('en', {style: "currency", currency: "USD"}) + '</td>' +
                            '</tr>';

                    }
                    $('#vc-table').find('tbody').html(html);
                    var theight = $(window).height() - 350;
                    $('#vc-table').dataTable({
                        "bJQueryUI": false,
                        "bAutoWidth": false,
                        "sPaginationType": "full_numbers",
                        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                        "iDisplayLength": -1,
                        "aoColumns": [ null,  null, {'sType': 'currency'}, {'sType': 'currency'}, {'sType': 'currency'}, {'sType': 'currency'},{'sType': 'currency'},{'sType': 'currency'},{'sType': 'currency'}, null, {'sType': 'currency'}],
                        "sDom": '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                        "oLanguage": {
                            "sSearch": "<span>Filter:</span> _INPUT_",
                            "sLengthMenu": "<span>Show entries:</span> _MENU_",
                            "oPaginate": {"sFirst": "First", "sLast": "Last", "sNext": ">", "sPrevious": "<"}
                        },
                        "scrollY" : theight+"px",
                    });
                    $(".dataTables_length select").select2({
                        minimumResultsForSearch: "-1"
                    });
                    get_table_totals('#vc-table');
                } else {
                    $('.table').addClass('has-padding').html(get_warning_message('There is no client spend data available to display.'));
                }
            },
            error:function(){
                $('.table').addClass('has-padding').html(get_error_message('An error occurred while fetching client spend data.'));
            }
        });
    }
function export_pdf(){
    
    $('.page-loader').slideDown('fast');    
    if($('.livc').hasClass("active")===false) $('.livc').hide();
    if($('.ligoogle').hasClass("active")===false) $('.ligoogle').hide();
    if($('.libing').hasClass("active")===false) $('.libing').hide();
    if($('.lifacebook').hasClass("active")===false) $('.lifacebook').hide();
    if($('.livideo').hasClass("active")===false) $('.livideo').hide();
    $('#apply_date').hide();
    $('#exportPDF').hide();
    $('#month').hide();
    $('#year').hide();

    var print_obj = $('div.page-content');
    var div = document.createElement('div');
    div.style.width = '8in';
    div.id = 'print_div';
    div.innerHTML = print_obj.html();
    var html = {html: div.innerHTML, fileName:'SEM_Campaign_Manager',pageType:'landscape'};
    $.ajax({
        url: '/pdf/export_pdf',
        data: html,
        method: 'POST',
        dataType: 'json',
        success: function(data) {
            $('.page-loader').slideUp('fast');
            $('.livc').show();
            $('.ligoogle').show();
            $('.libing').show();
            $('.lifacebook').show();
            $('.livideo').show();
            $('#apply_date').show();
            $('#exportPDF').show();
            $('#month').show();
            $('#year').show();
            if(data.result==1){
                window.location = '<?=base_url()?>pdf/download_pdf?file='+data.pdfPath;
            }else{
                bootbox.alert({
                    message: 'Unable to generate pdf.',
                    title: 'Error'
                });
            }            
        },
        error: function(){
            bootbox.alert({
                message: 'An error occured while trying to export this page to pdf.',
                title: 'Error'
            });
        }
    });
}
</script>