//Get Current and Previous Month Summary
function change_date(){
    var month = $('#month').val();
    var year = $('#year').val();
    get_summary(month, year);
    get_all_transactions(month, year);
}
function get_summary(month, year, cid){
    if(cid === undefined){$('.debits, .credits, .balance, .rollover, .payments, .admin, .net, .spent').html(ajax_loader);};
    $.ajax({
        url: '/advertising/transactions/get_summary?month='+month+'&year='+year+'&cid='+cid+'&ajax=true',
        dataType: 'JSON',
        success: function(data){
            $('.prev-month-label').html(data['prev_month'].label);
            $('.curr-month-label').html(data['curr_month'].label);
            if(cid !=undefined){
                var client = $('#client_'+cid);
                client.find('.prev-debits').html(Number(data['prev_month'].debits).toLocaleString('en', {style: 'currency', currency: 'USD'}));
                client.find('.prev-credits').html(Number(data['prev_month'].credits).toLocaleString('en', {style: 'currency', currency: 'USD'}));
                client.find('.prev-balance').html(Number(data['prev_month'].balance).toLocaleString('en', {style: 'currency', currency: 'USD'}));

                client.find('.curr-debits').html(Number(data['curr_month'].debits).toLocaleString('en', {style: 'currency', currency: 'USD'}));
                client.find('.curr-credits').html(Number(data['curr_month'].credits).toLocaleString('en', {style: 'currency', currency: 'USD'}));
                client.find('.curr-balance').html(Number(data['curr_month'].balance).toLocaleString('en', {style: 'currency', currency: 'USD'}));
            } else {
                var html ='';
                var p = $('#prev-month');
                var c = $('#curr-month');
                c.find('.debits').html(Number(data['curr_month'].debits).toLocaleString('en', {style: 'currency', currency: 'USD'}));
                c.find('.credits').html(Number(data['curr_month'].credits).toLocaleString('en', {style: 'currency', currency: 'USD'}));
                c.find('.balance').html(Number(data['curr_month'].balance).toLocaleString('en', {style: 'currency', currency: 'USD'}));
                c.find('.rollover').html(Number(data['curr_month'].rollover).toLocaleString('en', {style: 'currency', currency: 'USD'}));
                c.find('.payments').html(Number(data['curr_month'].payments).toLocaleString('en', {style: 'currency', currency: 'USD'}));
                c.find('.admin').html(Number(data['curr_month'].admin).toLocaleString('en', {style: 'currency', currency: 'USD'}));
                c.find('.net').html(Number(data['curr_month'].net_total).toLocaleString('en', {style: 'currency', currency: 'USD'}));
                c.find('.spent').html(Number(data['curr_month'].total_spent).toLocaleString('en', {style: 'currency', currency: 'USD'}));

                p.find('.debits').html(Number(data['prev_month'].debits).toLocaleString('en', {style: 'currency', currency: 'USD'}));
                p.find('.credits').html(Number(data['prev_month'].credits).toLocaleString('en', {style: 'currency', currency: 'USD'}));
                p.find('.balance').html(Number(data['prev_month'].balance).toLocaleString('en', {style: 'currency', currency: 'USD'}));
                p.find('.rollover').html(Number(data['prev_month'].rollover).toLocaleString('en', {style: 'currency', currency: 'USD'}));
                p.find('.payments').html(Number(data['prev_month'].payments).toLocaleString('en', {style: 'currency', currency: 'USD'}));
                p.find('.admin').html(Number(data['prev_month'].admin).toLocaleString('en', {style: 'currency', currency: 'USD'}));
                p.find('.net').html(Number(data['prev_month'].net_total).toLocaleString('en', {style: 'currency', currency: 'USD'}));
                p.find('.spent').html(Number(data['prev_month'].total_spent).toLocaleString('en', {style: 'currency', currency: 'USD'}));
            }
        },
        error: function(){
            $('#trans-table').find('tbody').html('<tr><td colspan="6">'+get_error_message('An error occurred while fetching the data.')+'</td>');
        }
    });

}
//Get Daily Data for Current Month
function get_all_transactions(month, year, cid){
    if ( $.fn.DataTable.isDataTable( '#trans-table' ) ) {
        $('#trans-table').dataTable().fnDestroy();
    }
    $('#trans-table').find('tbody').html('<tr><td scope="row" colspan="6">'+ajax_loader+'</td>');
    $.ajax({
        url: '/advertising/transactions/get_transactions?month='+month+'&year='+year+'&cid='+cid,
        dataType: 'JSON',
        success: function(data){
            var html ='';
            if(data.success == 1) {
                for (var i = 0; i < data['trans'].length; i++) {
                    html += '<tr id="trans-' + data['trans'][i].id + '">' +
                        '<td scope="row" class="date">' + data['trans'][i].entered_at + '</td>' +
                        '<td class="amount">' + Number(data['trans'][i].amount).toLocaleString('en', {style: 'currency', currency: 'USD'}) + '</td>' +
                        '<td class="platform">' + data['trans'][i].platform + '</td>' +
                        '<td class="type">' + data['trans'][i].trans_type + '</td>' +
                        '<td class="entered-by">' + data['trans'][i].entered_by + '</td>' +
                        '<td class="notes">' + data['trans'][i].notes + '</td>' +
                        '<td class="edit"><a href="javascript:void(0)" onclick="edit_transaction(' + data['trans'][i].id + ', '+cid+')" class="label label-info">Edit</a></td>' +
                        '</tr>';
                }
                $('#trans-table').find('tbody').html(html);
                $('#trans-table').dataTable({
                    "bJQueryUI": false,
                    "bAutoWidth": false,
                    "sPaginationType": "full_numbers",
                    "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    "iDisplayLength": -1,
                    "sDom": '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                    "oLanguage": {
                        "sSearch": "<span>Filter:</span> _INPUT_",
                        "sLengthMenu": "<span>Show entries:</span> _MENU_",
                        "oPaginate": {"sFirst": "First", "sLast": "Last", "sNext": ">", "sPrevious": "<"}
                    }
                });
                $(".dataTables_length select").select2({
                    minimumResultsForSearch: "-1"
                });
            } else {
                $('#trans-table').find('tbody').html('<tr><td colspan="6">'+get_warning_message(data['message'])+'</td>');
            }

        },
        error: function(){
            $('#trans-table').find('tbody').html('<tr><td colspan="6">'+get_error_message('An error occurred while fetching the data.')+'</td>');
        }
    });

}

//Get transaction types
function get_all_transaction_types(){
    $('#type-container').html(ajax_loader);
    $.ajax({
        url: '/advertising/transactions/get_transaction_types',
        dataType: 'JSON',
        success: function(data){
            var html = '<select id="trans_type" name="trans_type" class="select">';
            for(var i = 0; i < data.length; i++){
                html += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
            }
            html += '</select>';

            $('#form_modal').find('#type-container').html(html);
            $('#trans_type').select2({minimumResultsForSearch: '-1'})
        },
        error: function(){
            var html = '<span class="label label-danger form-control-inline">An error occurred while fetching transaction types.</span>';
            return html;
        }

    });

}
//Get all platforms
function get_all_platforms(){
    $.ajax({
        url: '/advertising/transactions/get_all_platforms',
        dataType: 'JSON',
        success: function(data){
            var html = '<select id="platform" name="platform" class="select-full">'+
                        '<option value="">All</option>';
            for(var i = 0; i < data.length; i++){
                html += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
            }
            html += '</select>';

            $('#form_modal').find('#platform-container').html(html);
            $('#platform').select2({minimumResultsForSearch: '-1', width: '100%'})
        }
    });
}
function show_hide_admin(type){

    if(type ===1){
        $('#admin-container').show();
    } else {
        $('#admin-container').hide();
        $('input#admin').val(0);
    }
}
//Get  a specific Transaction by id
function get_transaction(id){
    $.ajax({
        url:'/advertising/transactions/get_transaction_by_id?id='+id,
        dataType: 'JSON',
        success: function(data){
            return data;
        },
        error: function(){
            bootbox.alert({
                message: 'An error occurred while fetching the transaction data.',
                title: 'Error'
            });
        }
    });
}

//Open up modal to add new transaction
function add_transaction(cid){
    $('#add_transaction').trigger('reset');
    if(cid != undefined){$('#form_modal').find('input[name=client_id]').val(cid);}
    $('#form_modal').modal('show');
}

//Open up modal to edit existing transaction.
function edit_transaction(id, cid){
    $.ajax({
        url: '/advertising/transactions/get_transaction_by_id?id='+id,
        dataType: 'JSON',
        success: function(data){
            if(cid != undefined && $('#form_modal').find('input[name=client_id]') != undefined){ $('#form_modal').find('input[name=client_id]').val(cid);}
            $('#form_modal').find('#submit-btn').attr('onclick', 'submit_transaction('+id+')');
            $('#form_modal').find('#amount').val(Number(data.amount).toFixed(2));
            $('#form_modal').find('#trans_type').val(data.trans_type).trigger('change');
            $('#form_modal').find('#platform').val(data.platform);
            $('#form_modal').find('#notes').val(data.notes);
            $('#form_modal').modal('show');
        },
        error: function(){
            bootbox.alert({
                message: 'An error occurred while fetching the data.',
                title: 'Error'
            });
        }
    });
}

//Save Transaction Transaction
function submit_transaction(id){
    var month = $('#month').val();
    var year = $('#year').val();
    var cid = $('#form_modal').find('input[name=client_id]').val();
    var formData = $('#add_transaction').serializeArray();
    formData.push({name: 'month', value: $('#month').val()});
    formData.push({name: 'year', value: $('#year').val()});
    if(id == undefined){
        $.ajax({
            url: '/advertising/transactions/save_transaction',
            data: formData,
            method: 'POST',
            dataType: 'JSON',
            success: function(data){

                $('#form_modal').modal('hide');
                bootbox.alert({
                    message: 'Transaction successfully added.',
                    title: 'Success'
                });
                get_all_transactions(month, year);
                get_summary(month, year, cid);
            },
            error: function(){
                bootbox.alert({
                    message: 'An error occurred while saving the data. Please try again.',
                    title: 'Error'
                });
            }
        });
    } else {
        formData.push({name: 'id', value: id});
        $.ajax({
            url: '/advertising/transactions/update_transaction',
            data: formData,
            method: 'POST',
            dataType: 'JSON',
            success: function(data){

                $('#form_modal').modal('hide');
                bootbox.alert({
                    message: 'Transaction successfully updated.',
                    title: 'Success'
                });
                get_all_transactions(month, year, cid);
                get_summary(month, year, cid);
            },
            error: function(){
                bootbox.alert({
                    message: 'An error occurred while saving the data. Please try again.',
                    title: 'Error'
                });
            }
        })
    }
}

function get_all_clients(month, year){
    if ( $.fn.DataTable.isDataTable( '#vc-table' ) ) {
        $('#vc-table').dataTable().fnDestroy();
    }
    $('#vc-table').find('tbody').html('<tr><td scope="row" colspan="9">'+ajax_loader+'</td>');
    $.ajax({
        url: '/advertising/transactions/get_all_clients?month='+month+'&year='+year,
        dataType: 'json',
        success: function(data){
            var month = $('#month').val();
            var year = $('#year').val();
            var html = '';
            $('.prev-month-label').html(data.prev_month_label);
            $('.curr-month-label').html(data.curr_month_label);
            console.log(data['clients'].length);
            for(var i = 0; i < data['clients'].length; i++){
                var client = data['clients'][i];
                html += '<tr id="client_'+client.client_id+'" class="client-row">'+
                        '<td scope="row"><a href="javascript:void(0)" onclick="expand_client_table('+client.client_id+', this)"><i class="fa fa-caret-right"></i></a> '+client.client_code+' -  '+client.client_name+'</td>'+
                        '<td><span class="label '+client.team_class+'">'+client.team_name+'</span></td>'+
                        '<td class="prev-debits">'+Number(client.summary.prev_month.debits).toLocaleString('en-US', {style: 'currency', currency: 'USD'}) +'</td>'+
                        '<td class="prev-credits">'+Number(client.summary.prev_month.credits).toLocaleString('en-US', {style: 'currency', currency: 'USD'})+'</td>'+
                        '<td class="prev-balance">'+Number(client.summary.prev_month.balance).toLocaleString('en-US', {style: 'currency', currency: 'USD'})+'</td>'+
                        '<td class="curr-debits">'+Number(client.summary.curr_month.debits).toLocaleString('en-US', {style: 'currency', currency: 'USD'})+'</td>'+
                        '<td class="curr-credits">'+Number(client.summary.curr_month.credits).toLocaleString('en-US', {style: 'currency', currency: 'USD'})+'</td>'+
                        '<td class="curr-balance">'+Number(client.summary.curr_month.balance).toLocaleString('en-US', {style: 'currency', currency: 'USD'})+'</td>'+
                        '<td class="text-center"><a role="button" href="javascript:void(0)" class="btn-xs btn btn-info" onclick="add_transaction('+client.client_id+')">Add Payment</a></td>'+
                        '</tr>';

            }
            $('#vc-table').find('tbody').html(html);
            $('#vc-table').dataTable({
                "bJQueryUI": false,
                "bAutoWidth": false,
                "sPaginationType": "full_numbers",
                "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "iDisplayLength": -1,
                "sDom": '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                "oLanguage": {
                    "sSearch": "<span>Filter:</span> _INPUT_",
                    "sLengthMenu": "<span>Show entries:</span> _MENU_",
                    "oPaginate": {"sFirst": "First", "sLast": "Last", "sNext": ">", "sPrevious": "<"}
                }
            });
            $(".dataTables_length select").select2({
                minimumResultsForSearch: "-1"
            });
        },
        error: function(){
            $('#vc-table').find('tbody').html('<tr><td scope="row" colspan="9">'+get_error_message('An error occurred while fetching the data.')+'</td>');
        }
    });
}

function expand_client_table(cid, obj){
    collapse_client_tables();
    $('tr#client_'+cid).find('i').removeClass('fa fa-caret-right').addClass('fa fa-caret-down');
    $(obj).attr('onclick', 'collapse_client_tables('+cid+', this)');
    var html = '<tr id="trans_'+cid+'" class="client-details">'+
                    '<td colspan="9" style="padding:0;">'+
                        '<table class="table table-bordered table-striped" id="trans-table">'+
                            '<thead>'+
                                '<tr>'+
                                    '<th scope="col">Date</th>'+
                                    '<th scope="col">Amount</th>'+
                                    '<th scope="col">Platform</th>'+
                                    '<th scope="col">Type</th>'+
                                    '<th scope="col">Auth. By</th>'+
                                    '<th scope="col">Notes</th>'+
                                    '<th scope="col"></th>'+
                                '</tr>'+
                            '</thead>'+
                            '<tbody>'+
                                '<tr><td scope="row" colspan="6">'+ajax_loader+'</td></tr>'+
                            '</tbody>'+
                        '</table>'+
                    '</td>'+
                '</tr>';
    $('#vc-table').find('tr#client_'+cid).after(html);
    var month = $('#month').val();
    var year = $('#year').val();

    get_all_transactions(month, year, cid);

}
function collapse_client_tables(cid, obj){
    if(cid != undefined){
        $('#trans_'+cid).remove();
        $(obj).attr('onclick', 'expand_client_table('+cid+', this)');
        $(obj).find('i').removeClass('fa fa-caret-down').addClass('fa fa-caret-right');
    } else {
        $('.client-details').remove();
        $('.client-row').find('i').removeClass('fa fa-caret-down').addClass('fa fa-caret-right');
    }
}
function change_dom_date(){
    var month = $('#month').val();
    var year = $('#year').val();
    get_all_clients(month, year);
}

