========================
SEM Budget Manager
——————————————
Project: 	Contology
Framework: 	Codeigniter
========================

Description
——————————————
A budget manager to allow the SEM deparment to manage all clients SEM spending within the web application. We pull sem spending data from google, bing and facebook apis nightly through a cron job and save in either an xml file or the database depending on the source. 

From there we calculate multiple forumulas like daily and montly spending requirements, based on accounting data within the app. 

Notes
----------------
The model was ommited because it was built by another team member.