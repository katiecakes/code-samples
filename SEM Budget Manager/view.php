<?=page_loader();?>
<div class="page-content">
    <?php notifyError(); ?>
    <div class="page-title">
        <h5>SEM Campaign Manager: <span><?= get_client_name();?></span></h5>
        <div class="pull-right form-inline">
            <select name="timeFrame" class="select-liquid" id="month" placeholder="Select Time Frame">
                <?php
                $month_options = '';
                $curr_month;
                $curr_month = date('m');
                for( $i = 1; $i <= 12; $i++ ) {
                    $month_num = str_pad( $i, 2, 0, STR_PAD_LEFT );
                    $year=date('Y');
                    $month_name = date( 'F',strtotime($year."/".$i."/25"));
                    $month_options .= '<option '.($curr_month == $month_num ? 'selected="selected"' : '').' value="' . $month_num . '">' . $month_name . '</option>';
                }
                echo $month_options;
                ?>
            </select>            
            <select class="select-liquid" id="year">
                <?= '<option selected="selected" value="'.date('Y').'">'.date('Y').'</option><option value="'.date("Y",strtotime("-1 year")).'">'.date("Y",strtotime("-1 year")).'</option>'; ?>
            </select>
            <button type="button" id="apply_date" class="btn btn-info">Apply</button>
            <button id="exportPDF" class="btn btn-primary pull-right" title="Export to PDF" onclick="export_pdf()"><i class="fa fa-file-pdf-o"></i> PDF</button>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h6 class="panel-title">Summary</h6>
        </div>
        <div id="month-summary" class="panel-body">
            <?=ajax_loader()?>
        </div>
    </div>
    <ul class="nav nav-pills">
        <li class="active livc" data-rel="#vc"><a href="javascript:toggle_table('#vc')">VC</a></li>
        <li class="ligoogle" data-rel="#google"><a href="javascript:toggle_table('#google')">Google</a></li>
        <li class="libing" data-rel="#bing"><a href="javascript:toggle_table('#bing')">Bing</a></li>
        <li class="lifacebook" data-rel="#facebook"><a href="javascript:toggle_table('#facebook')">Facebook</a></li>
        <!--<li class="livideo" data-rel="#video"><a href="javascript:toggle_table('#video')">Video</a></li>-->
    </ul>
    <div id="vc" class="panel panel-default faux-tab">
        <table id="vc-table" class=" table table-striped table-bordered">
            <thead>
            <tr>
                <th scope="col">Client</th>
                <th scope="col">Team</th>
                <th scope="col">Rollover</th>
                <th scope="col">Amount Received</th>
                <th scope="col">Admin Fee</th>
                <th scope="col" title="Roll + Received - Admin Fee">Net Budget to be Allocated</th>
                <th scope="col">Elapsed Days in Month</th>
                <th scope="col" title="(Budget / Days in Month) x Elapsed Days">MTD Target Spend</th>
                <th scope="col">Actual Spend for Month</th>
                <th scope="col" title="Target Spend / Should">MTD Variance</th>
                <th scope="col" title="(Budget - Spent)/ (Days in Month / Elapsed Days)">New Daily Spend Target</th>
                <th scope="col">Notes</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="13"><?=ajax_loader()?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="clearfix"></div>
    <?php
    $month = $curr_month;
    $year = date('Y');
    $days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
    $locale = 'en_US';
    $nf = new NumberFormatter($locale, NumberFormatter::ORDINAL);
    $thead = '';
    $j = 1;
    for($i = 1; $i<=$days; $i++){
        $thead .= '<th scope="col">' . $nf->format($i) . '</th>';
        $j++;
    }
    $total_days = $j;
    ?>
    <div id="google" class="panel panel-default faux-tab">
        <table id="google-table" class="table table-striped">
            <thead>
            <tr>
                <th scope="col" style="width:250px !important;">Client<span style="width:250px; visibility:hidden; display:block;"></span></th>
                <th scope="col">Team</th>
                <?= $thead ?>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td scope="row" colspan="<?=$total_days;?>"><?=ajax_loader()?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="clearfix"></div>
    <div id="bing" class="panel panel-default faux-tab">
        <div class="panel-heading"><h6 class="panel panel-title">Bing</h6></div>
        <table id="bing-table" class="table table-striped">
            <thead>
            <tr>
                <th scope="col" style="width:250px !important;">Client<span style="width:250px; visibility:hidden; display:block;"></span></th>
                <th scope="col">Team</th>
                <?= $thead ?>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td scope="row" colspan="<?= $total_days?>"><?=ajax_loader()?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="clearfix"></div>
    <div id="facebook" class="panel panel-default faux-tab">
        <div class="panel-heading"><h6 class="panel-title">Facebook</h6></div>
        <table id="facebook-table" class="table  table-striped">
            <thead>
            <tr>
                <th scope="col" style="width:250px !important;">Client<span style="width:250px; visibility:hidden; display:block;"></span></th>
                <th scope="col">Team</th>
                <?=$thead?>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td scope="row" colspan="<?=$total_days;?>"><?=ajax_loader()?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <!--<div id="video" class="tab-pane fade in">
        <div class="table">
            <table class="table  table-striped">
                <thead>
                <tr>
                    <th scope="col" style="width:250px !important;">Client<span style="width:250px; visibility:hidden; display:block;"></span></th>
                    <th scope="col">Team</th>
                    <?=$thead?>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td scope="row" colspan="<?=$total_days;?>"><?=ajax_loader()?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>-->

</div>
<div id="note-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 class="modal-title">SEM Note</h5>
            </div>
            <form id="note-form" class="form-horizontal">
                <input id="note-id" type="hidden" name="id" value="">
                <input id="note-month" type="hidden" name="month" value="">
                <input id="note-year" type="hidden" name="year" value="">
                <input id="note-cid" type="hidden" name="cid" value="">
                <div class="modal-body has-padding">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label class="sr-only" for="note">Note</label>
                            <textarea id="note" name="note" class="form-control" placeholder="Enter note..."></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button id="submit-note" type="button" onclick="submit_note()" class="btn btn-info">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?=base_url()?>js/ad_manager.js"></script>
<script type="text/javascript">
    var active_tab = '#vc';
$(document).ready(function(){
    $('.page-loader').slideDown();
    var month = $('#month').val();
    var year = $('#year').val();
    get_all_budgets(month, year);
    $('#apply_date').click(function(){
        var month = $('#month').val();
        var year = $('#year').val();
        change_date(month, year)
    });
    get_facebook_spend(month, year);
    get_google_spend(month, year);
    get_bing_spend(month, year);
    get_video_spend(month, year);
    check_tables()
});
function toggle_table(table){
    $('div.panel.faux-tab').hide();
    $('div'+table).show();
    $('.nav-pills li').removeClass('active');
    $('.nav-pills li[data-rel='+table+']').addClass('active');
    $(window).trigger('resize');

}
var timer = 0;
function check_tables(){

    var vc = $('#vc-table');
    var google = $('#google-table');
    var facebook = $('#facebook-table');
    var bing = $('#bing-table');
    if($.fn.DataTable.fnIsDataTable(vc)){
        $('.page-loader').slideUp();
        toggle_table(active_tab);
    } else if(timer < 20000){
        timer = timer + 100;
        setTimeout(check_tables, 100);
    }else{
        $('.page-loader').slideUp();
        bootbox.alert({
            message: 'Tables failed to load. Please try again.',
            title: 'Error'
        });
    }
}
function export_pdf(){
    
    $('.page-loader').slideDown('fast');    
    if($('.livc').hasClass("active")===false) $('.livc').hide();
    if($('.ligoogle').hasClass("active")===false) $('.ligoogle').hide();
    if($('.libing').hasClass("active")===false) $('.libing').hide();
    if($('.lifacebook').hasClass("active")===false) $('.lifacebook').hide();
    if($('.livideo').hasClass("active")===false) $('.livideo').hide();
    $('#apply_date').hide();
    $('#exportPDF').hide();
    $('#month').hide();
    $('#year').hide();

    var print_obj = $('div.page-content');
    var div = document.createElement('div');
    div.style.width = '8in';
    div.id = 'print_div';
    div.innerHTML = print_obj.html();
    var html = {html: div.innerHTML, fileName:'SEM_Campaign_Manager',pageType:'landscape'};
    $.ajax({
        url: '/pdf/export_pdf',
        data: html,
        method: 'POST',
        dataType: 'json',
        success: function(data) {
            $('.page-loader').slideUp('fast');
            $('.livc').show();
            $('.ligoogle').show();
            $('.libing').show();
            $('.lifacebook').show();
            $('.livideo').show();
            $('#apply_date').show();
            $('#exportPDF').show();
            $('#month').show();
            $('#year').show();
            if(data.result==1){
                window.location = '<?=base_url()?>pdf/download_pdf?file='+data.pdfPath;
            }else{
                bootbox.alert({
                    message: 'Unable to generate pdf.',
                    title: 'Error'
                });
            }            
        },
        error: function(){
            bootbox.alert({
                message: 'An error occured while trying to export this page to pdf.',
                title: 'Error'
            });
        }
    });
}
</script>