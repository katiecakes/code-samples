/**For the media Library Module**/

/**
 * Upload an item.
 */
function upload(pub){
    $('#page-loader').slideDown();
    $('#upload_form').validate();
    $('#tags').blur();
    var form = $('#upload_form')[0];
    var formData =  new FormData(form);
    if($('#job_id').length > 0){
        formData.append('job', $('#job_id').val());
    }
    $.ajax({
       url: '/mediaLib/create',
       data: formData,
       fileElementId	:'file',
       method: 'POST',
       cache: false,
       contentType: false,
       processData: false,
        dataType: 'JSON',
       success: function(data){
           if(data.status ===1) {
               display_media(data)
           }else{
               bootbox.alert({
                   message: data.message,
                   title: 'Error'
               });
           }

           $('#page-loader').slideUp();
       },
       error: function(xhr, status, error){
           var err = eval("(" + xhr.responseText + ")");
           alert(err.Message);
           console.log(error);
       }
    });
}

/**
 * Display freshly uploaded file in the target container.
 * @param data = array
 */
function display_media(data, pub){
    var html ='';
    if(pub === true){
        html += '<div data-id="' + data.id + '" class="col-xs-4 col-sm-2 col-md-1 media-item text-center">' +
            '<img class="imageReplace" src="' + data.file + '" data-id="' + data.id + '" title="' + data.title + '" alt="'+data.title+'" />' +
            '</div>';
    } else {
        html = '<div data-id="'+data.id+'" class="col-xs-4 col-sm-2 col-md-1 media-item text-center">'+
            '<a href="javascript:void(0)" onclick="media_details('+data.id+')">'+
            '<img src="'+data.file+'" data-id="'+data.id+'" class="img-responsive" title="'+data.alt+'" />'+
            '</a>'+
            '</div>';
    }
    $('#media-container').prepend(html);
    square_obj('div[data-id="'+data.id+'"]');
}

/**
 * Calc height for image previews
 *
 * @param target = element to square.
 */
function square_obj(target){
    var width = $(target).width();
    $(target).height(width);
}

/**
 * Search Media
 */
function search_media(pub){

    var term = $('#search').val();
    var month = $('#month').val();
    var year = $('#year').val();
    if(month.length === 0){ month =false;}
    if(year.length === 0){year = false;}
    $('#media-container').html(ajax_loader);
    $.ajax({
        url: '/mediaLib/search_media',
        data: {term: term, month: month, year: year},
        method: 'POST',
        dataType: 'JSON',
        success:function(data){
            var html ='';
            if(data){
                if(pub === true){
                    $.each(data, function(i, v){
                        html += '<div data-id="' + v.id + '" class="col-xs-4 col-sm-2 col-md-1 media-item text-center">' +
                                    '<img class="imageReplace" src="' + v.url + '" data-id="' + v.id + '" title="' + v.title + '" alt="'+v.title+'" />' +
                                '</div>';
                    });
                } else {
                    $.each(data, function(i, v){
                        html += '<div data-id="' + v.id + '" class="col-xs-4 col-sm-2 col-md-1 media-item text-center">' +
                            '<a href="javascript:void(0)" onclick="media_details(' +v.id + ')">' +
                            '<img src="' + v.url + '" data-id="' + v.id + '" title="' + v.title + '" />' +
                            '</a>' +
                            '</div>';
                    });
                }

            } else {
                html = get_error_message('An error occurred while searching the data.');
            }
            $('#media-container').html(html);
            square_obj('.media-item');
        },
        error: function(){
            bootbox.alert({
                title: 'Error',
                message: 'An error occurred while fetching the data.'
            });
        }
    });
}

/**
 * Swaps layout of thumbnails
 * @param target
 * @param obj
 * @TODO: possible feature in future.
 */
function swap_layout(target, obj){
    $(obj).parent().find('button').removeClass('active');
    $(obj).addClass('active');
    get_all_media(target);
}
/**
 * Gets a list of all media
 * @param target
 * @TODO: Remove once we've finished testing since we will never get all media at once.
 */
function get_all_media(target){
    var params = [];
    var layout = $('#layout').find('.active').data('type');
    console.log(layout);
    params = {
        start: $('#start').val(),
        end: $('#end').val(),
        search: $('#search').val()
    };
    $(target).html(ajax_loader);
    $.ajax({
        url: '/mediaLib/get_all_media',
        dataType: 'JSON',
        success: function(data){
            switch(layout) {
                case 'small':
                    var html = '<div class="row">';
                    for (var i = 0; i < data.length; i++) {
                        html += '<div data-id="' + data[i].id + '" class="col-xs-3 col-sm-2 col-md-1 media-item text-center">' +
                                    '<a href="javascript:void(0)" onclick="media_details(' + data[i].id + ')">' +
                                        '<img src="' + data[i]['url'] + '" data-id="' + data[i].id + '" title="' + data[i].title + '" />' +
                                    '</a>' +
                                '</div>';
                    }
                    html += '</div>';
                    break;
                case 'large':
                    var html = '<div class="row">';
                    for (var i = 0; i < data.length; i++) {
                        html += '<div data-id="' + data[i].id + '" class="col-xs-6 col-sm-3 col-md-2 media-item text-center">' +
                                    '<a href="javascript:void(0)" onclick="media_details(' + data[i].id + ')">' +
                                        '<img src="' + data[i]['url'] + '" data-id="' + data[i].id + '" title="' + data[i].title + '" />' +
                                    '</a>' +
                                '</div>';
                    }
                    html += '</div>';
                    break;
                case 'list':
                    var html = '';
                    for (var i = 0; i < data.length; i++) {
                        html += '<div class="row">' +
                                    '<div data-id="' + data[i].id + '" class="col-xs-3 col-sm-2 col-md-1 media-item text-center">' +
                                        '<a href="javascript:void(0)" onclick="media_details(' + data[i].id + ')">' +
                                            '<img src="' + data[i]['url'] + '" data-id="' + data[i].id + '" title="' + data[i].title + '" />' +
                                        '</a>' +
                                    '</div>'+
                                    '<div class="col-xs-9 col-sm-10 col-md-11">'+data[i].title+'</div>'+
                                '</div>';
                    }
                    break;
                default:
                    var html = '<div class="row">';
                    for (var i = 0; i < data.length; i++) {
                        html += '<div data-id="' + data[i].id + '" class="col-xs-3 col-sm-2 col-md-1 media-item text-center">' +
                                    '<a href="javascript:void(0)" onclick="media_details(' + data[i].id + ')">' +
                                        '<img src="' + data[i]['url'] + '" data-id="' + data[i].id + '" title="' + data[i].title + '" />' +
                                    '</a>' +
                                '</div>';
                    }
                    html += '</div>';
            }
            $(target).html(html);
            square_obj('.media-item');
        },
        error: function(){
            bootbox.alert({
                title: 'Error',
                message: 'An error occurred while fetching the data.'
            });
            $(target).html(get_error_message('An error occurred while fetching the data.'));
        }
    });
}

/**
 * Gets the details of the selected media.
 * @param id
 */
function media_details(id){
    $.ajax({
        url: '/mediaLib/media_details',
        data: {id: id},
        method: 'POST',
        dataType: 'JSON',
        success: function(data){
            var modal = $('#media-detail');
            modal.find('#id').val(data.id);
            modal.find('#file-name').text(data.filename);
            modal.find('#file-type').text(data.filetype);
            modal.find('#file-size').text(data.filesize);
            modal.find('#media-width').text(data.width);
            modal.find('#media-height').text(data.height);
            modal.find('#uploaded-on').text(data.created);
            modal.find('#image-container').html('<img src="'+data.url+'" alt="'+data.alt+'" class="img-responsive">');
            modal.find('#url').val(data.url);
            modal.find('#title').val(data.title);
            modal.find('#desc').val(data.desc);
            modal.find('#caption').val(data.caption);
            modal.find('#alt').val(data.alt);
            modal.find('#uploaded-by').text(data.uploaded_by);
            modal.find('#update-button').attr('onclick', 'update_media('+data.id+')');
            modal.find('#delete_media').attr('onclick', 'delete_media('+data.id+')');
            var str = '';
            for(var i = 0; i<data.tags.length; i++){
                str += data.tags[i].Name+',';
            }
            modal.find('#tags-container').html('<input id="tags" name="tags" style="width:100% !important;" value="'+str+'" />');
            modal.find('#tags').tagsInput({
                autocomplete_url:'/mediaLib/get_tags',
                    selectFirst:true,
                    autoFill:true,
                width: 'auto'
            });
            modal.modal('show');
        },
        error: function(xhr, status, error){
            bootbox.alert({
                message: 'Something went wrong while fetching the data.',
                title: 'Error'
            });
        }
    });
}

/**
 * Update Media Details
 * @param id
 */
function update_media(id){
    $('#update-form').find('#tags').blur();
    var form = $('#update-form')[0];
    var formData =  new FormData(form);

    $.ajax({
        url: 'mediaLib/update_media',
        data: formData,
        method: 'POST',
        dataType: 'JSON',
        processData: false,
        contentType: false,
        success: function(data){
            if(data.status === 1){
                bootbox.alert({
                    message: 'Details successfully updated.',
                    title: 'Success'
                });
            } else {
                bootbox.alert({
                    message: data.message,
                    title: 'Error'
                });
            }
        },
        error: function(xhr, status, error){
            console.log(error);
        }
    });
}

/**
 * Delete an item
 * @param id
 */
function delete_media(id){
    bootbox.confirm({
        message: 'Are you sure you want to delete this item?',
        title: 'Confirm Delete',
        callback: function(r){
            if(r){
                $.ajax({
                    url: '/mediaLib/delete_media',
                    data: {id: id},
                    method: 'POST',
                    dataType: 'JSON',
                    success: function(data){
                        $('div[data-id="'+id+'"').remove();
                        $('#media-detail').modal('hide');
                        if(data.success === 1 || data === true){
                            $('#media-detail').modal('hide');
                            $('div[data-id='+id+']').remove();
                            bootbox.alert({
                                message: 'Media successfully deleted.',
                                title: 'Success'
                            });

                        }
                    }
                });
            }
        }
    });
}

/**
 * Delete multiple media.
 * @TODO: Possible feature in future.
 */
function bulk_delete(){

}