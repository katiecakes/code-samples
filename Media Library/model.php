<?php
/**
 * COPYRIGHT DOM360
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CloudMedia
 *
 * Model used to manage media library data.
 *
 * @author KA  <kaustin@dom360.com>
 * @version 2017-10-25
 * @package Models
 *
 */
class CloudMedia extends DOM_Model
{
    /**
     * CloudMedia constructor.
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Inserts media into database.
     * @param $id
     * @param $data
     * @return bool
     */
    public function set_media($data)
    {
        if (!empty($data)) {
            if(!empty($data['id'] && $data['id'] > 0)){
                if($this->db->where('id', $data['id'])->update('MediaLib', $data)){

                    return $data['id'];
                }
            } else {
                if ($this->db->insert("MediaLib", $data)) {
                    $id = $this->db->insert_id();
                    return  $id;
                }
            }
        } else {
            return FALSE;
        }
    }

    /**
     * Update a media's tags
     *
     * First delete all tags associated with the media, then batch insert the new assocations
     *
     * @param int $id media id for which we will query information
     * @param array $tags array of tags to attach to the article
     *
     * @table MediaTags
     */
    public function update_media_tags($id, $tags)
    {
        //first delete all tags
        $r = $this->db->from('MediaLibTags')->where('media_id', $id)->delete();
        //add new tags
        foreach ($tags as $t) {
            if ($t && strlen($t) > 0) {
                $tag_id = $this->insert_tag($t);
                $this->db->insert('MediaLibTags', array('media_id' => $id, 'tag_id' => $tag_id));
            }
        }
        return $r;
    }

    /**
     * Insert a tag
     *
     * @param string $tag tag that we will search for and add if it does not exist
     *
     * @table Tags
     *
     * @return int|bool if the tag exists or is successfully created return the tag id, otherwise return FALSE.
     */
    public function insert_tag($tag)
    {
        if ($tag) {
            //check if tag already exists, if so just return ID
            $query = $this->db->select('id')->from('MediaTags')->where('name', $tag)->limit(1)->get();
            if ($query && $query->num_rows() > 0 && $query->row()->id) return $query->row()->id;

            //else need to insert new tag and return new ID
            if ($this->db->insert('MediaTags', array('name' => $tag))) return $this->db->insert_id();
        }
        return FALSE;
    }

    /**
     * Get all tags
     *
     * @param string $str optional - searches for tag like $str
     *
     * @table Tags
     *
     * @return string[] array of tag strings.
     */
    public function get_tags($str = FALSE)
    {
        $tags = array();
        $this->db->select('name')->from('MediaTags');
        if ($str && strlen($str) > 0) $this->db->like('name', $str);
        $query = $this->db->get();
        if ($query && $query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $tags[] = $row->name;
            }
        }

        return $tags;
    }

    /**
     * Gets the file name of a media item.
     *
     * @param $id
     * @return bool |array
     */
    public function get_filename($id){
        $r = false;
        $q = $this->db->select('file_name')
                ->from('MediaLib')
                ->where('id', $id)
                ->get();
        if($q && $q->num_rows() > 0){
            $r = $q->result();
        }
        return $r;
    }

    /**
     * Deletes media from db and removes tag entries from MediaTags
     * @table MediaLib
     * @table MediaLibTags
     * @param $id = media id
     * @return bool
     */
    function delete_media($id){
        $r = false;
        $q = false;
        $t = $this->db->where('media_id', $id)->delete('MediaLibTags');
        //If tag unlinks are successful delete media entry from db.
        if($t == true){
            $q = $this->db->where('id', $id)->delete('MediaLib');
        }
        if($q && $t == true){
            $r = true;
        }
        return $r;
    }

    /**
     * Gets all media in media library
     * @table MediaLib
     * @return bool | array
     */
    public function get_all_media(){
        $r = false;
        $q = $this->db->select('id, filename')
                ->from('MediaLib')
                ->get();
        if($q && $q->num_rows() > 0 ){
            $r = $q->result_array();
        }
        return $r;
    }
    /** Get overview by id
     * @table MediaLib
     * @param $id
     * @return bool | array
     */
    public function get_preview($id){
        $r = false;
        $q = $this->db->select('id, title, caption, filename')
                ->from('MediaLib')
                ->where('id', $id)
                ->get();
        if($q && $q->num_rows() > 0){
            $r = $q->row_array();
        }
        return $r;
    }

    /**
     * Gets the details of a media item.
     *
     * @param $id = media id
     * @return bool | array
     */
    public function get_media_details($id){
        $r = false;
        $q = $this->db->select('*')
            ->from('MediaLib')
            ->where('id', $id)
            ->get();
        if($q && $q->num_rows() > 0 ){
            $r = $q->row_array();
        }
        return $r;
    }

    /**
     * Gets all media items uploaded by a specific user.
     *
     * @param $uid
     * @return bool | array
     */
    public function get_media_by_user($uid){
        $r = false;
        $q = $this->db->select('*')
            ->from('MediaLib')
            ->where('user_id', $uid)
            ->get();
        if($q && $q->num_rows() > 0 ){
            $r = $q->result();
        }
        return $r;
    }

    /**
     * Gets all media that was uploaded to a specific client.
     * @param $cid
     * @return bool | array
     */
    public function get_media_by_client($cid){

    }

    /**
     * Gets the tags related to a specific media item.
     * @param $mid = media id
     * @return bool | array
     */
    public function get_media_tags($mid){
        $r = false;
        $q = $this->db->select('MediaTags.Name')
                ->from('MediaLibTags')
                ->where('media_id', $mid)
                ->join('MediaTags', 'MediaLibTags.tag_id = MediaTags.id')
                ->get();
        if($q && $q->num_rows() > 0){
            $r = $q->result_array();
        }
        return $r;
    }

    /** Search Media
     * @param $str = string
     * @param $month = int
     * @param $year = int
     * @return array
     *
     * @TODO filter results by Month/Year.
     */
    public function search_media($str, $month, $year){
        $mq = '';
        $yq = '';
        $sq = '';
        $query = "SELECT distinct(id) FROM MediaLib M";

        if($month != 'false'){
            $query .= " WHERE month = '".$month."'";
        }
        if($month !='false' && $year !='false'){
            $query .= " AND";
        } else if($month =='false' && $year != 'false'){
            $query .= " WHERE";
        }
        if($year !='false'){
            $query .= " year = '".$year."'";
        }
        if(strlen($str) > 0){
            if($month !='false' || $year != 'false'){
                $query .= " AND";
            } else if ($month == 'false' && $year == 'false'){
                $query .= " WHERE";
            }
            $query  .= " LOWER(M.title) like '%".$str."%'
                    OR LOWER(M.desc) like '%".$str."%'
                    OR LOWER(M.caption) like '%".$str."%'";
        }
        $q = $this->db->query($query);


        if($q && $q->num_rows() > 0) {
            return $q->result();
        } else {
            return FALSE;
        }
    }

    /** Search Media Tags
     * @param $str = comma sep string
     * @oaram $month = int
     * @param $year = int
     * @return array
     *
     * @TODO filter results by Month/Year.
     */

    public function search_tags($str){
        $q = $this->db->query("SELECT Media_id
          FROM MediaLibTags mlt 
          WHERE mlt.tag_id in (
          SELECT GROUP_CONCAT(id) 
          FROM MediaTags mt 
          WHERE LOWER(mt.name) like ('%".$str."%'))");

        if($q && $q->num_rows() > 0){
            return $q->result();
        } else {
            return FALSE;
        }
    }
}