<?php

/**
 * COPYRIGHT DOM360
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Contology Media Library Controller
 *
 * @author KA <kaustin@dom360.com>
 * @version 2017-10-15
 * @package Controllers
 *
 * @TODO: Define who has access to this module.
 *
 */
class MediaLib extends DOM_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('CloudMedia', 'cm');
        $this->load->model(['domclients', 'domsocial', 'administration']);
        $this->load->library('assets');

        $this->client_id = $this->user['DropdownDefault']->SelectedClient;
        $this->group_id = $this->user['DropdownDefault']->SelectedGroup;
        //Who has access?
        if(!$this->checkPermission('MediaLib',true)){
            throwError(newError("Publisher", 2, 'Access Denied to Media Library. You do not have the correct permissions to view this section.', 1, ''));
            redirect(base_url(), 'refresh');
        }
    }

    /**
     * Load the view.
     *
     */
    public function index()
    {
        if($this->client_id){
            $data['search'] = $this->domclients->get_client_code($this->client_id);
        } else if(!$this->client_id && $this->group_id !=56){
            $data['search'] = $this->domclients->get_group_name($this->group_id);
        } else {
            $data['search'] = '';
        }

        $this->loadTemplate('pages/media/overview', $data);
    }

    /**
    * Add a media item
    * @return mixed
    */
    public function create()
    {
        $data = $this->input->post();
        $result = false;
        if ($_FILES['file'] && $_FILES['file']['error'] == FALSE) {
            $origFileName = explode(".", $_FILES['file']['name']);
            $newFileName = $this->user['UserID'] . '_' . time() . '_' . uniqid() . '.' . pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $rootpath = $_SERVER['DOCUMENT_ROOT'];
            $upload_config['upload_path'] = $rootpath . '/imgs/c';
            $upload_config['allowed_types'] = 'png|jpg|jpeg|gif';
            $upload_config['file_name'] = $newFileName;
            $upload_config['overwrite'] = 'FALSE';
            $upload_config['file_name'] = time() . uniqid() . '.' . pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

            $image_info = getimagesize($_FILES['file']['tmp_name']);
            $filesize = filesize($_FILES['file']['tmp_name']);
            $filetype = mime_content_type($_FILES['file']['tmp_name']);
            $image_width = $image_info[0];
            $image_height = $image_info[1];

            $res = $this->assets->do_upload('file', $upload_config, 'medialib', $newFileName);
            if(is_array($this->group_id)){
                $pg = $this->group_id[0];
            }

            if ($res) {
                $saveData = [
                    'client_id' => $this->client_id,
                    'group_id'  => $pg,
                    'user_id'   => $this->user['UserID'],
                    'filename' => $newFileName,
                    'created'   => time(),
                    'month'     => date('n'),
                    'year'      => date('Y'),
                    'width'     => $image_width,
                    'height'    => $image_height,
                    'title'     => $this->input->post('title', TRUE),
                    'alt'       => $this->input->post('alt', TRUE),
                    'caption'   => $this->input->post('caption', TRUE),
                    'desc'      => $this->input->post('desc', TRUE),
                    'filesize'  => $filesize,
                    'filetype'  => $filetype,

                ];
                $mid = $this->cm->set_media($saveData);
                    if($mid != FALSE) {
                        $tags = explode(',', $this->input->post('tags'));
                        $job = FALSE;
                        if($this->input->post('job', TRUE)){
                            $job = $this->input->post('job', TRUE);
                        }else if($this->input->get('job', TRUE)){
                            $job = $this->input->get('job', TRUE);
                        }
                        //Autotag the job number if uploading within a publisher job.

                        if($job !=FALSE){
                            array_push($tags, $this->domclients->get_client_code($this->client_id).'-'.$job);
                        }
                        //Autotag basic info
                        if($this->client_id){
                            array_push($tags, $this->domclients->get_client_code($this->client_id));
                            array_push($tags, $this->domclients->get_client_name($this->client_id));
                            if(is_array($this->group_id)){
                                foreach($this->group_id as $g){
                                    array_push($tags, $this->domclients->get_group_name($g));
                                }
                            }

                        } else {
                            array_push($tags, $this->domclients->get_group_name($this->group_id));
                        }

                        array_push($tags, date('F'));
                        array_push($tags, date('Y'));

                        $this->cm->update_media_tags($mid, $tags);

                        $fileLocation = $this->assets->get_dir('medialib/' . $newFileName);
                        $result = ['status' => 1, 'message' => 'Success', 'id' => $mid, 'file' => $fileLocation];
                    } else {
                        $remove = $this->assets->remove('medialib', $newFileName);
                        $result = ['status' => 0, 'message' => 'Unable to save data.', 'fileRemoved' => $remove];
                    }
            } else {
                $result = ['status' => 0, 'message' => 'Unable to upload file.'];
            }
        } else {
            $result = array(
                'status' => 0,
                'message' => 'You are not allowed to upload files.'
            );
        }
        echo json_encode($result);
    }
    /**
     * Delete media item
     *
     * @POST id | array of ids
     * @model CloudMedia
     * @return bool
     */
    public function delete_media()
    {
        $r = false;
        $id = $this->input->post('id', true);

        $file = $this->cm->get_filename($id);
        $rem = $this->assets->remove('/medialib', $file);
        if($rem){
             $r = $this->cm->delete_media($id);
        }
        echo json_encode($r);

    }
    /**
     * Gets an array of images for browsing.
     *
     * @return array
     */
    public function get_all_media()
    {
        $media = $this->cm->get_all_media();
        for($i=0; $i < count($media); $i++){
            $media[$i]['url'] = $this->assets->get('medialib', $media[$i]['filename']);
            $tags = $this->cm->get_media_tags($media[$i]['id']);
            $media[$i]['tags'] = $tags;
        }
        echo json_encode($media);
    }
    /**
     * Gets details of an image.
     *
     * @POST id
     * @return array
     */
    public function media_details($id = FALSE, $ajax = FALSE)
    {

        if($id == FALSE){
            $id = $this->input->post('id', TRUE);
            $ajax = TRUE;
        }
        $data = $this->cm->get_media_details($id);
        $mime = finfo_open(FILEINFO_MIME_TYPE);;
        $data['url'] = $this->assets->get('medialib', $data['filename']);
        $data['tags'] =  $this->cm->get_media_tags($id);
        $data['created'] = date('m/d/Y h:i a', $data['created']);
        $data['uploaded_by'] = $this->members->get_user_contact_name($data['user_id']);

        echo json_encode($data);
    }
    /**
     * Update the details of a media item.
     *
     * @POST arr
     * @return bool
     */
    public function update_media()
    {
        $r = [
            'status' => 0,
            'message' => 'Could not update media info.'
        ];
        $data = $this->input->post(NULL, FALSE);
        $tags = explode(',', $data['tags']);
        unset($data['tags']);
        $res = $this->cm->set_media($data);

        if($res != false && $res > 0){
            $tres = $this->cm->update_media_tags($data['id'], $tags);
            if($tres != false){
                $r['status'] = 1;
                $r['message'] = 'Success';
                $r['tags'] = $tres;
            } else {
                $r['status'] = 1;
                $r['message'] = 'Could not update tags.';
                $r['tags'] = $tres;
            }
        }
        echo json_encode($r);
    }

    /**
     * Fetch a list of tags for autocompleting a tags input for an article
     *
     * @get term >> the term for which to return a list of tags for autocompletion
     *
     * @model Tags::get_ags()
     *
     * @return json list of tags as strings that match the given term
     */
    public function get_tags()
    {
        $res = array();
        $str = $this->input->get('term', TRUE);
        $tags = $this->cm->get_tags($str);
        foreach($tags as $t)
        {
            $res[] = array('id'=>$t,'label'=>$t,'value'=>$t);
        }
        echo json_encode($res);
    }

    /**
     * Search media library
     *
     * @post search string, date params
     *
     * @model CloudMedia::search_media()
     *
     * @return json array of results
     */

    public function search_media(){
        $r = array();
        $tags = array();
        $str = $this->input->post('term', TRUE);
        $month = $this->input->post('month', TRUE);
        $year = $this->input->post('year', TRUE);
        $tags = str_replace(' ', ',', $str);
        $mq = $this->cm->search_media($str, $month, $year);
        if($mq !=FALSE){
            foreach ($mq as $key => $id){
                $r[$id->id] =  3;
            }
        }
        //Search the individual words of the string
        $terms = explode(',', $tags);
        foreach($terms as $t){
            $sq = $this->cm->search_media($t, $month, $year);
            if($sq !=FALSE){
                foreach($sq as $key => $id){
                    if(array_key_exists($id->id, $r)){
                        $r[$id->id] = $r[$id->id]+1;
                    }else{
                        $r[$id->id] = 1;
                    }
                }
            }
        }
        //Search the individual words for any tags.
        $tq = $this->cm->search_tags($tags);
        if($tq !=FALSE){
            foreach($tq as $key => $id){
                if(array_key_exists($id->Media_id, $r)){
                    $r[$id->Media_id] = $r[$id->Media_id]+1;
                } else {
                    $r[$id->Media_id] = 1;
                }
            }
        }
        //Order results by significance.
        arsort($r);
        $f = [];
        foreach($r as $key=>$val){
            $details = $this->cm->get_preview($key);
            $f[$key] = $details;
            $f[$key]['url'] = $this->assets->get('medialib', $details['filename']);
        }
        echo json_encode($f);



    }


}