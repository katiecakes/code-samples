<?=page_loader()?>
<link href="<?= base_url() . $CSSDIR; ?>dropzone.css" rel="stylesheet" type="text/css">
<div class="page-content">
    <div class="page-title">
        <h5>Media Library</h5>
        <button class="btn btn-lg btn-primary pull-right" onclick="toggle_upload()" >Upload</button>
    </div>
    <div id="upload" class="tabbable page-tabs hidden">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#single" data-toggle="tab">Upload</a></li>
            <li><a href="#bulk" data-toggle="tab">Bulk Import</a></li>
        </ul>
        <div class="tab-content has-padding">
            <div id="single" class="tab-pane active fade in">
                <form id="upload_form" class="validate form-horizontal" novalidate="novalidate"><div class="form-group">
                        <label for="file" class="control-label col-sm-4">File:</label>
                        <div class="col-sm-8"><input id="file" name="file" type="file" class="styled required"></div>
                    </div>
                    <div class="form-group">
                        <label for="title" class="control-label col-sm-4">Title:</label>
                        <div class="col-sm-8"><input id="title" name="title" class="form-control required"></div>
                    </div>
                    <div class="form-group">
                        <label for="caption" class="control-label col-sm-4">Caption:</label>
                        <div class="col-sm-8"><input id="caption" name="caption" class="form-control"></div>
                    </div>
                    <div class="form-group">
                        <label for="alt" class="control-label col-sm-4">Alt Text:</label>
                        <div class="col-sm-8"><input id="alt" name="alt" class="form-control"></div>
                    </div>
                    <div class="form-group">
                        <label for="desc" class="control-label col-sm-4">Description:</label>
                        <div class="col-sm-8"><textarea id="desc" name="desc" class="form-control"></textarea></div>
                    </div>
                    <div class="form-group">
                        <label for="tags" class="control-label col-sm-4">Tags:</label>
                        <div id="type_container" class="col-sm-8">
                            <input id="tags" name="tags">
                        </div>
                    </div>
                    <div class="form-group">
                        <div id="upload_msg" class="col-sm-9"></div>
                        <div class="col-sm-3 text-right">
                            <button type="button" onclick="upload()" class="btn btn-info">Upload</button>
                            <button type="button" onclick="toggle_upload()" class="btn btn-default">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
            <div id="bulk" class="tab-pane fade">
                <div id="bulk-uploader" class="dropzone"></div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div id="layout" class="col-md-5 col-sm-4 col-xs-12">
                    <h6>Filter &amp; Search:</h6>
                    <!--<button type="button" data-type="small" class="btn btn-link btn-icon btn-sm tip active" onclick="swap_layout('#media-container', this)" title="Small Grid"><i class="fa fa-th"></i></button>
                    <button type="button" data-type="large" class="btn btn-link btn-icon btn-sm tip" onclick="swap_layout('#media-container', this)" title="Large Grid"><i class="fa fa-th-large"></i></button>
                    <button type="button" data-type="list" class="btn btn-link btn-icon btn-sm tip" onclick="swap_layout('#media-container', this)" title="List"><i class="fa fa-list"></i></button>-->
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="row">
                        <div class="col-xs-6">
                            <!--<input id="start" class="datepicker form-control">-->
                            <select id="month" name="month" class="select-full">
                                <option value="">All</option>
                                <?php for($m = 1; $m <= 12; $m++){
                                    $mt = mktime(0,0,0,$m, 1, date('Y'));
                                    echo '<option value="'.date('n', $mt).'" '.($m == date('n') ? 'selected="selected"' : '').'>'.date('F', $mt).'</option>';
                                }?>
                            </select>
                        </div>
                        <div class="col-xs-6">
                            <!--<input id="end" class="datepicker form-control">-->
                            <select id="year" name="year" class="select-full">
                                <option value="">All</option>
                                <?php
                                $y = date('Y');
                                for($i = 0; $i < 5; $i++){
                                    $year = $y-$i;
                                    echo '<option value="'.$year.'" '.($year == date('Y') ? 'selected="selected"' : '').'>'.$year.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-x-12 ">
                    <input id="search" type="text" name="search" placeholder="Enter Search Term" value="<?=$search?>" class="form-control">
                </div>
                <div class="col-sm-1 col-xs-12">
                    <button type="button" onclick="search_media()" class="btn btn-info btn-block"><i class="fa fa-search"></i><span class="hidden-sm">Go</span></button>
                </div>
            </div>
        </div>

    </div>

    <div class="panel panel-default">
        <div class="panel-heading"></div>
        <div id="media-container" class="panel-body">
            <?=ajax_loader()?>
        </div>
    </div>
    <!--Media Details Modal-->
    <div id="media-detail" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h5 class="modal-title">Media Details</h5>
                </div>

                <div class="modal-body has-padding">
                    <div id="image-container" class="col-sm-7">

                    </div>
                    <div class="col-sm-5 form-horizontal">
                        <div class="row">
                            <div class="col-xs-12">
                                <strong>File name:</strong> <span id="file-name"></span><br />
                                <strong>File Type:</strong> <span id="file-type"></span><br />
                                <strong>Uploaded on: </strong> <span id="uploaded-on"></span><br />
                                <strong>File size:</strong> <span id="file-size"></span><br />
                                <strong>Dimensions:</strong> <span id="media-width"></span> x <span id="media-height"></span>
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <label for="url" class="control-label col-sm-4 text-right">URL</label>
                            <div class="col-sm-8"><input id="url" name="url" type="url" readonly class="form-control" onClick="this.select();" /></div>
                        </div>
                        <form id="update-form">
                            <input type="hidden" id="id" name="id"/>
                            <div class="form-group">
                                <label for="title" class="control-label col-sm-4 text-right">Title</label>
                                <div class="col-sm-8"><input id="title" name="title" type="text" class="form-control" /></div>
                            </div>
                            <div class="form-group">
                                <label for="caption" class="control-label col-sm-4 text-right">Caption</label>
                                <div class="col-sm-8"><input id="caption" name="caption" type="text" class="form-control" /></div>
                            </div>
                            <div class="form-group">
                                <label for="alt" class="control-label col-sm-4 text-right">Alt Text</label>
                                <div class="col-sm-8"><input id="alt" name="alt" type="text" class="form-control" /></div>
                            </div>
                            <div class="form-group">
                                <label for="desc" class="control-label col-sm-4 text-right">Description</label>
                                <div class="col-sm-8"><textarea name="desc" id="desc" class="form-control"></textarea></div>
                            </div>
                            <div class="form-group">
                                <label for="tags" class="control-label col-sm-4 text-right">Tags</label>
                                <div id="tags-container" class="col-sm-8"><input name="tags" id="tags" class="form-control"></div>
                            </div>
                        </form>
                        <div class="form-group">
                            <label for="uploaded-by" class="control-label col-sm-4 text-right">Uploaded By</label>
                            <div id="uploaded-by" class="col-sm-8 form-control-static"></div>
                        </div>
                        <hr />
                        <div class="row">
                            <a id="delete_media" href="javascript:void(0)" class="text-danger" onclick="delete_media()">Delete Permanently</a>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-warning" data-dismiss="modal">Close</button>
                    <button id="update-button" class="btn btn-primary" >Update</button>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="<?= base_url() ?>js/mediaLib.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/plugins/dropzone.js"></script>
<script>
    square_obj('.media-item');
    Dropzone.autoDiscover = false;
    var myDropzone = new Dropzone("div#bulk-uploader", { url: "/mediaLib/create"});
    myDropzone.on("success", function(file, response) {
        myDropzone.removeFile(file);
        var data = JSON.parse(response);
        display_media(data);
    });
    $('#tags').tagsInput({
        autocomplete_url:'/mediaLib/get_tags',
        autocomplete:{selectFirst:true,'width':'300px',autoFill:true}
    });
    search_media('#media-container');
    window.addEventListener('resize', function(event){
        square_obj('.media-item');
    });
    window.dispatchEvent(new Event('resize'));
    $(window).load( function(){
        window.dispatchEvent(new Event('resize'));
        square_obj('.media-item');
    });
    $('#month, #year').on('change', search_media());
    function toggle_upload(){
        $('#upload').toggleClass('hidden');
    }
</script>
