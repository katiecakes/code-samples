========================
Media Library Module
——————————————
Project: 	Contology
Framework: 	Codeigniter
========================

Description
——————————————
A media library based on the Wordpress media manager. A user is able to upload and categorize by tagging a media item with identifiable terms. The system also automatically tags pertinent information, month, date, year to save the user time and allows consistency across all users. 