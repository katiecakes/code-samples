import React, { Component, Fragment } from "react";
import { Flex, Box } from "@rebass/grid";
import styled from "styled-components";
import MessageList from "Views/Chat/MessageList";
import ChatWindow from "Views/Chat/ChatWindow";
import NewMessage from "Views/Chat/NewMessage";
import ChatInput from "Views/Chat/ChatInput";
import Icon from "Components/Utilities/Icon/Icon";
import moment from "moment";
//import { studentActions } from "../../Store/reducers/StudentReducer";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
/*import {
  selectStudents,
  selectedStudentAppbar
} from "../../Store/selectors/students.selectors";*/

export default class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeChat: 1,
      chatData: chatData,
      chatWindowActive: false
    };
    this.changeActiveChat = this.changeActiveChat.bind(this);
    this.sendMessage = this.sendMessage.bind(this);
    this.deleteMessage = this.deleteMessage.bind(this);
    this.addMessage = this.addMessage.bind(this);
  }
  searchMessages(term) {
    //Do something here.
  }
  getContacts(term) {
    //For adding recipients, do an autocomplete onchange.
  }
  archiveMessage(id) {
    if (window.confirm("Are you sure you want to archive this message?")) {
      let chatData = this.state.chatData;
      this.setState({ activeChat: 0 });
      delete chatData.chat[id];
      this.setState({ chatData: chatData });
      return true;
    } else {
      return false;
    }
  }
  deleteMessage(id) {
    if (window.confirm("Are you sure you want to delete this message?")) {
      let chatData = this.state.chatData;
      this.setState({ activeChat: 0 });
      delete chatData.chat[id];
      this.setState({ chatData: chatData });
      return true;
    } else {
      return false;
    }
  }
  addMessage(recipients, msg) {
    let cid = Math.floor(Math.random() * 100);
    let users = {
      267: {
        id: 267,
        avatarUrl: "https://randomuser.me/api/portraits/women/68.jpg",
        name: "Susan Davis"
      }
    };
    Object.entries(recipients).map(([i, r]) => {
      users[i] = r;
      return true;
    });
    let chatData = this.state.chatData;
    chatData.chat.push({
      cid: cid,
      active: true,
      lastUpdated: "0 mins ago",
      users: users,
      messages: [
        {
          id: Math.floor(Math.random() * 500),
          chatId: cid,
          senderId: 267,
          timestamp: moment()
            .format("h:mm a")
            .toString(),
          text: msg
        }
      ]
    });
    this.setState({
      chatData: chatData,
      chatWindowActive: true
    });
    this.changeActiveChat(this.state.chatData.chat.length - 1);
  }
  sendMessage(activeChat, msg) {
    let cid = Math.floor(Math.random() * 100);
    let chatData = this.state.chatData;
    chatData.chat[activeChat].messages.push({
      id: cid,
      chatId: activeChat,
      senderId: chatData.currentUser,
      timestamp: moment()
        .format("h:mm a")
        .toString(),
      text: msg
    });
    this.setState({ chatData: chatData });
  }
  changeActiveChat(id) {
    const chatData = this.state.chatData;
    if (chatData.chat[id].unread === true) {
      chatData.chat[id].unread = false;
      this.setState({ chatData: chatData });
    }
    this.setState({
      activeChat: id,
      chatWindowActive: true
    });
  }
  componentDidMount() {
    //this.props.getStudents();
  }
  render(props) {
    const { selectedStudent } = this.props;
    //let { avatar } = selectedStudent;
    return (
      <Container>
        <MobileHeader>
          <Flex flexWrap="wrap">
            <Box width={1} />
            <h2>Messages</h2>
            <NewMessage addMessage={this.addMessage} />
          </Flex>
        </MobileHeader>
        <Flex flexWrap="wrap">
          <MessageListContainer width={[1, 1, 1 / 4]}>
            <MessageList
              data={this.state.chatData.chat}
              users={this.state.chatData.users}
              currentUser={this.state.chatData.currentUser}
              activeChat={this.state.activeChat}
              changeActiveChat={this.changeActiveChat}
              visible={true}
            />
          </MessageListContainer>
          <ChatWindowContainer
            width={[1, 1, 3 / 4]}
            active={this.state.chatWindowActive}
          >
            <Header>
              <Messages px={4} pb={3} pt={4}>
                <Box width={[1 / 2]}>
                  <h2>Messages</h2>
                </Box>
                <Box width={[1 / 2]} className="text-right">
                  <NewMessage addMessage={this.addMessage} />
                </Box>
              </Messages>
            </Header>
            {this.state.chatData.chat.length > 1 &&
            this.state.chatData.chat[this.state.activeChat] !== undefined ? (
              <Fragment>
                <ChatHeader px={4} py={2}>
                  <ToggleChat>
                    <Icon
                      type="tableft"
                      onClick={() => this.setState({ chatWindowActive: false })}
                      width={50}
                      height={50}
                    />
                  </ToggleChat>
                  <Box width={1}>
                    <span>Chat with</span>
                    {Object.entries(
                      this.state.chatData.chat[this.state.activeChat].users
                    ).map(
                      ([key, user]) =>
                        this.state.chatData.currentUser !== user.id && (
                          <h2 key={key}>{user.name}</h2>
                        )
                    )}
                  </Box>
                  <ChatAction
                    onClick={() => this.archiveMessage(this.state.activeChat)}
                  >
                    <Icon
                      type="archive"
                      color="#A5A7AB"
                      width={25}
                      height={25}
                    />
                  </ChatAction>
                  <ChatAction
                    onClick={() => this.deleteMessage(this.state.activeChat)}
                  >
                    <Icon type="trash" color="#A5A7AB" width={25} height={25} />
                  </ChatAction>
                </ChatHeader>
                <ChatWindow
                  avatar={'https://randomuser.me/api/portraits/women/90.jpg'}
                  data={this.state.chatData.chat[this.state.activeChat]}
                  cid={this.state.activeChat}
                  currentUser={this.state.chatData.currentUser}
                />
                <ChatInput
                  avatar={'https://randomuser.me/api/portraits/women/90.jpg'}
                  activeChat={this.state.activeChat}
                  currentUser={this.state.chatData.currentUser}
                  sendMessage={this.sendMessage}
                />
              </Fragment>
            ) : (
              <div className="text-center subdue">
                <h1>Please create a new conversation to start.</h1>
              </div>
            )}
          </ChatWindowContainer>
        </Flex>
      </Container>
    );
  }
}
const MobileHeader = styled.div`
  display: none;
  @media screen and (max-width: 992px) {
    display: block;
    padding: 0 20px;
  }
`;
const ChatAction = styled.div`
  margin: 25px;
  cursor: pointer;
  float: right;
  display: inline-block;
`;
const ChatHeader = styled(Flex)`
  border-top: 1px solid #cad3df;
  border-bottom: 1px solid #cad3df;
  background-color: #edf0f6;
  span {
    opacity: 0.4;
    font-size: 12px;
  }
`;
const Container = styled.div`
  margin: -40px -30px -30px -30px;
  @media screen and (max-width: 992px) {
    margin: 0 -8px;
    padding: 0;
  }
`;
const ToggleChat = styled.div`
  display: none;
  cursor: pointer;
  @media screen and (max-width: 992px) {
    padding: 25px 0 0 0;
    display: block;
  }
`;
const MessageListContainer = styled(Box)`
  height: calc(100vh - 72px);
  overflow: hidden;
  border-right: 1px solid ${props => props.theme.borderColorOne};
  @media screen and (max-width: 992px) {
    position: fixed;
    height: calc(100vh - 150px);
    width: calc(100vw - 100px);
    margin-left: -20px;
  }
  @media screen and (max-width: 767px) {
    width: 100vw;
    margin-left: 0;
    height: calc(100vh - 140px);
  }
`;
const ChatWindowContainer = styled(Box)`
  height: calc(100vh - 72px);
  position: relative;
  overflow: hidden;
  @media screen and (max-width: 992px) {
    position: fixed;
    left: ${props => (props.active ? "auto" : "1000px")};
    height: calc(100vh - 150px);
    width: calc(100vw - 100px);
    margin-left: -20px;
  }
  @media screen and (max-width: 767px) {
    left: ${props => (props.active ? "0" : "800px")};
    width: 100vw;
    height: calc(100vh - 140px);
    margin-left: 0;
  }
`;
const Messages = styled(Flex)`
.button {
  float: right;
  display: flex;
  align-items: center;
  width: 134px;
  color: #fff;
  font-weight: 600;
  font-size: 16px;
  padding: 6px 16px;
  border: none;
  border-radius: 5px;
  background-color: #8648FB;
  cursor: pointer;
  .buttonText {
    margin-left: 12px;
  }
  `;

const Header = styled.div`
  display: block;
  @media screen and (max-width: 992px) {
    display: none;d
  }
`;

const chatData = {
  currentUser: 267,
  chat: [
    {
      id: 1,
      lastUpdated: "5 min ago",
      active: true,
      unread: true,
      users: {
        267: {
          id: 267,
          avatarUrl: "https://randomuser.me/api/portraits/women/68.jpg",
          name: "Susan Davis"
        },
        937: {
          id: 937,
          avatarUrl: "https://randomuser.me/api/portraits/men/49.jpg",
          name: "Frank Baker"
        }
      },
      messages: [
        {
          id: 43,
          chatId: 1,
          senderId: 267,
          timestamp: "09:04 am",
          text:
            "Hey Susan, lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec nibh ipsum. Aliquam gravida diam non fringilla ultricies. Donec maximus purus ac elit tristique interdum."
        },
        {
          id: 44,
          chatId: 1,
          senderId: 937,
          timestamp: "09:04 am",
          text:
            "Hey Susan, lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec nibh ipsum. Aliquam gravida diam non fringilla ultricies. Donec maximus purus ac elit tristique interdum."
        }
      ]
    },
    {
      id: 2,
      lastUpdated: "5 min ago",
      unread: false,
      users: {
        267: {
          id: 267,
          avatarUrl: "https://randomuser.me/api/portraits/women/68.jpg",
          name: "Susan Davis"
        },
        937: {
          id: 937,
          avatarUrl: "https://randomuser.me/api/portraits/women/49.jpg",
          name: "Sara Irvine"
        }
      },
      messages: [
        {
          id: 43,
          chatId: 2,
          senderId: 267,
          timestamp: "09:04 am",
          text:
            "Hey Susan, lorem sdfadfasdfasdfasdfasdfasfsdfsipsum dolor sit amet, consectetur adipiscing elit. Sed nec nibh ipsum. Aliquam gravida diam non fringilla ultricies. Donec maximus purus ac elit tristique interdum."
        },
        {
          id: 44,
          chatId: 2,
          senderId: 937,
          timestamp: "09:04 am",
          text:
            "Hey Susan, lorem ipsumdfasdfffffffffffffffffffffffffffffffff dolor sit amet, consectetur adipiscing elit. Sed nec nibh ipsum. Aliquam gravida diam non fringilla ultricies. Donec maximus purus ac elit tristique interdum."
        }
      ]
    }
  ]
};


