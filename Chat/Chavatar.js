import React from "react";
import styled from "styled-components";

const Chavatar = props => {
  return (
    <Avatar avatar={props.user.avatarUrl}>
      {!props.user.avatarUrl && props.user.name.charAt(0)}
    </Avatar>
  );
};
export default Chavatar;
const Avatar = styled.div`
  height: 40px;
  width: 40px;
  border-radius: 50%;
  background: ${props =>
    props.avatar
      ? `url(${props.avatar})`
      : "linear-gradient(135deg, #01d3ff 0%, #00a8ff 100%)"};
  background-size: contain;
`;
