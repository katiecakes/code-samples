import React, { Component, Fragment } from "react";
import { Flex, Box } from "@rebass/grid";
import ReactDOM from "react-dom";
import styled from "styled-components";
import Chavatar from "Views/Chat/Chavatar";

export default class ChatWindow extends Component {
  constructor() {
    super();
    this.state = {
      message: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentWillUpdate() {
    const node = ReactDOM.findDOMNode(this);
    this.shouldScrollToBottom =
      node.scrollTop + node.clientHeight + 100 >= node.scrollHeight;
  }

  componentDidUpdate() {
    if (this.shouldScrollToBottom) {
      const node = ReactDOM.findDOMNode(this);
      node.scrollTop = node.scrollHeight;
    }
  }
  handleChange(e) {
    this.setState({
      message: e.target.value
    });
  }
  handleSubmit(e) {
    e.preventDefault();
    this.props.sendMessage(this.props.cid, this.state.message);
    this.setState({
      message: ""
    });
  }
  render() {
    return (
      <Fragment>
        <ChatBox>
          {this.props.data.messages.length > 0 ? (
            this.props.data.messages.map((msg, i) => {
              return (
                <Flex
                  px={4}
                  py={3}
                  key={i}
                  flexWrap="noWrap"
                  className={
                    this.props.currentUser === msg.senderId
                      ? "currentUserMessage"
                      : "otherUser"
                  }
                >
                  <Box px={2} width={[1 / 10, 1 / 10, 0.5 / 8]}>
                    <Chavatar user={this.props.data.users[msg.senderId]} />
                  </Box>
                  <Box
                    px={2}
                    className="chatText"
                    width={[8 / 10, 8 / 10, 6 / 8]}
                  >
                    <p>{msg.text}</p>
                  </Box>
                  <Box px={2} width={[1 / 10, 1 / 10, 1 / 8]}>
                    <span>{msg.timestamp}</span>
                  </Box>
                </Flex>
              );
            })
          ) : (
            <div>
              No messages to display. Please select a conversation to view.
            </div>
          )}
        </ChatBox>
      </Fragment>
    );
  }
}
const ChatBox = styled.div`
  background-color: #fff;
  height: 60vh;
  overflow-y: scroll;
  padding-bottom: 10%;
  box-sizing: border-box;
  .userImage {
  }
  p {
    background-color: #8648fb;
    border-radius: 0 20px 20px 20px;
    color: #fff;
    padding: 20px;
  }
  span {
    color: #868e96;
    font-size: 14px;
  }
  .currentUserMessage {
    flex-direction: row-reverse;
    p {
      border-radius: 20px 0px 20px 20px;
      background-color: #676d79;
    }
    span {
      float: right;
    }
    .userImage {
      float: right;
    }
  }
`;
