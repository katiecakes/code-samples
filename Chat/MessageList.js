import React, { Component, Fragment } from "react";
import { Flex, Box } from "@rebass/grid";
import styled from "styled-components";
import Search from "../../Assets/Icons/Search.svg";
import Chavatar from "Views/Chat/Chavatar";
export default class MessagesList extends Component {
  render() {
    return (
      <Fragment>
        <ChatSearch>
          <input className="search" type="text" placeholder="Search..." />
          <div className="select">
            <select>
              <option>Hub Users</option>
              <option>BJU Press Instructors</option>
            </select>
            <div className="select_arrow" />
          </div>
        </ChatSearch>
        {this.props.data.length > 0 && this.props.data !== undefined ? (
          this.props.data.map((c, i) => {
            return (
              <ChatSelectorWrapper
                key={i}
                onClick={() => this.props.changeActiveChat(i)}
                data-value={this.props.activeChat === i ? "selected" : ""}
              >
                {/* data-value="selected" is used for when the user has clicked on the chat thread */}
                {/* <ChatSelector flexWrap="wrap" data-value="selected"> */}
                <ChatSelector flexWrap="wrap" p={2}>
                  <ChatSelectImage width={[1 / 7]}>
                    {Object.entries(c.users).map(([key, user]) => {
                      if (this.props.currentUser !== user.id) {
                        return <Chavatar key={key} user={user} />;
                      }
                      return true;
                    })}
                  </ChatSelectImage>
                  <ChatSelectText width={[6 / 7]} px={3}>
                    <div className="userInfo">
                      {Object.entries(c.users).map(([key, user]) => {
                        if (this.props.currentUser !== user.id) {
                          return (
                            <span className="name" key={key}>
                              {user.name}
                            </span>
                          );
                        }
                        return true;
                      })}
                      <span className="time">{c.lastUpdated}</span>
                    </div>
                    <p className="textEllipsized">
                      {c.unread === true ? (
                        <strong>
                          {c.messages[c.messages.length - 1].text}
                        </strong>
                      ) : (
                        c.messages[c.messages.length - 1].text
                      )}
                    </p>
                  </ChatSelectText>
                </ChatSelector>
              </ChatSelectorWrapper>
            );
          })
        ) : (
          <div>There are no conversations.</div>
        )}
      </Fragment>
    );
  }
}

const ChatSearch = styled(Box)`
  border-right: 1px solid #e3e3e3;
  .search {
    background: url(${Search}) no-repeat scroll 30px 28px;
    opacity: 0.75;
    background-size: 18px;
    font-family: "Nunito", sans-serif;
    font-size: 1rem;
    line-height: 1.75rem;
    font-weight: 600;
    background-color: transparent;
    border: 1px solid transparent;
    color: #343a40;
    box-sizing: border-box;
    height: 38px;
    width: 100%;
    padding: 38px 6px 30px 60px;
    border-bottom: 1px solid #e3e3e3;
  }
  .select {
    position: relative;
    display: inline-block;
    margin: 5px 0 8px 0;
    padding-left: 25px;
  }
  .select select {
    font-size: 16px;
    font-weight: 600;
    display: inline-block;
    width: 120%;
    cursor: pointer;
    padding: 7px;
    outline: 0;
    border: 0px hidden #000000;
    border-radius: 0px;
    background: transparent;
    color: #000000;
    appearance: none;
    -webkit-appearance: none;
    -moz-appearance: none;
  }
  .select select::-ms-expand {
    display: none;
  }
  .select select:hover,
  .select select:focus {
    color: #000000;
    background: transparent;
  }
  .select select:disabled {
    opacity: 0.5;
    pointer-events: none;
  }
  .select_arrow {
    position: absolute;
    top: 16px;
    right: -20px;
    pointer-events: none;
    border-style: solid;
    border-width: 5px 5px 0px 5px;
    border-color: #373a3c transparent transparent transparent;
  }
  .select select:hover ~ .select_arrow,
  .select select:focus ~ .select_arrow {
    border-top-color: #000000;
  }
  .select select:disabled ~ .select_arrow {
    border-top-color: #cccccc;
  }
`;

const ChatSelectorWrapper = styled.div`
  &[data-value="selected"] {
    background-color: #ffffff;
    box-shadow: 0 15px 30px 0 rgba(16, 27, 79, 0.15);
  }
  h2 {
    padding: 0;
    margin: 0;
  }
`;

const ChatSelector = styled(Flex)`
  box-sizing: border-box;
  border-top: 1px solid #e3e3e3;
  border-right: 1px solid #e3e3e3;
	cursor:pointer
  padding: 25px 8px 25px 32px !important;
  :hover {
    background-color: #ffffffad;
  }
`;

const ChatSelectImage = styled(Box)`
  .userImage {
    background: ${props =>
      props.userImage
        ? props => props.userImage
        : "linear-gradient(135deg, #01d3ff 0%, #00a8ff 100%)"};
    background-size: cover;
    background-position: center center;
    text-align: center;
    font-weight: bold;
    padding: 8px 5px;
    box-sizing: border-box;
    height: 40px;
    width: 40px;
    border-radius: 50%;
  }
`;
const ChatSelectText = styled(Box)`
  .userInfo {
    display: flex;
    justify-content: space-between;
  }
  .name,
  .time {
    opacity: 0.4;
    color: #000000;
    font-size: 12px;
  }
  .textEllipsized {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
`;
