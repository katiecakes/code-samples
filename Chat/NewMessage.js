import React, { Component, Fragment } from "react";
import Panel from "Components/Panel/Panel";
import { Flex, Box } from "@rebass/grid";
import Modal from "../../Components/Modal/Modal";
import Button from "@material/react-button";
import Icon from "Components/Utilities/Icon/Icon";
import Toggle from "../../Components/Utilities/Toggle/Toggle";
import styled from "styled-components";

export default class NewMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: "",
      modalOpen: false,
      recipients: {
        455: {
          id: 455,
          avatarUrl: "https://randomuser.me/api/portraits/women/52.jpg",
          name: "Cindy Laurence"
        }
      }
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
  }
  toggleModal = () => {
    this.setState({ modalOpen: !this.state.modalOpen });
  };

  handleChange(e) {
    this.setState({
      message: e.target.value
    });
  }
  handleSubmit(e) {
    e.preventDefault();
    this.props.addMessage(this.state.recipients, this.state.message);
    this.setState({
      message: ""
    });
    this.toggleModal();
  }
  render() {
    return (
      <Toggle>
        {({ on, toggle }) => (
          <Fragment>
            <Button
              style={{ position: "absolute", right: "20px" }}
              raised
              onClick={this.toggleModal}
              icon={
                <Icon type="newmessage" width={20} height={20} color="#fff" />
              }
            >
              New Message
            </Button>
            <Modal on={this.state.modalOpen} toggle={toggle}>
              <NewMessageModal>
                <Panel bodyPadding="0px" title="New Message">
                  <Flex alignItems="center" p={2} className="addRecipient">
                    <Box p={2} width={[1 / 8]}>
                      <div className="userImage" />
                    </Box>
                    <Box p={2} width={[6 / 8]}>
                      <div className="newChatRecipient">Cindy Laurence</div>
                    </Box>
                    <Box p={2} width={[1 / 8]}>
                      <Icon
                        type="addrecipient"
                        width={22.5}
                        height={25.5}
                        color="#A5A7AB"
                      />
                    </Box>
                  </Flex>
                  <div className="newChatTextArea">
                    <textarea
                      onChange={this.handleChange}
                      className="newChatTextArea"
                      defaultValue="Start a conversation."
                    />
                  </div>
                  <Flex justifyContent="flex-end" p={2}>
                    <Button onClick={this.toggleModal}>Cancel</Button>
                    <Button
                      raised
                      onClick={this.handleSubmit}
                      icon={
                        <Icon
                          type="sendmessage"
                          width={20}
                          height={20}
                          color="#fff"
                        />
                      }
                    >
                      Send
                    </Button>
                  </Flex>
                </Panel>
              </NewMessageModal>
            </Modal>
          </Fragment>
        )}
      </Toggle>
    );
  }
}
const NewMessageModal = styled.div`
  max-width: 600px;
  width: 90vw;
  height: 600px;
  .addRecipient {
    border-bottom: 1px solid #E3E3E3;
  }
  .userImage {
    height: 40px;
    width: 40px;
    border-radius: 50%;
    background: linear-gradient(135deg, #01D3FF 0%, #00A8FF 100%);
  }
  .newChatRecipient {
    font-size: 21px;
    font-weight: 600;
  }
  .newChatTextArea {
    padding: 10px;
    border-bottom: 1px solid #E3E3E3;
    
    textarea {
      width: 95%;
      height: 300px;
      background: transparent;
      border: none;
      outline: none;
      box-shadow: none;
      resize: none;
      color: #868E96;
      font-size: 16px;
    }
  }
  .button {
    display: flex;
    align-items: center;
    width: 68px;
    color: #fff;
    font-weight: 600;
    font-size: 16px;
    padding: 6px 16px;
    border: none;
    border-radius: 5px;
    background-color: #8648FB;
    cursor: pointer;
    .buttonText {
      margin-left: 12px;
    }
    `;
