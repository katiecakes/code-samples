import React, { Component } from "react";
import { Flex, Box } from "@rebass/grid";
import styled from "styled-components";
import Button from "@material/react-button";
import Icon from "../../Components/Utilities/Icon/Icon";

const lineHeight = 24;
export default class ChatInput extends Component {
  constructor() {
    super();
    this.state = {
      message: "",
      rows: 1
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
  }
  handleChange(e) {
    const oldRows = e.target.rows;
    e.target.rows = 1;
    const newRows = ~~(e.target.scrollHeight / lineHeight);
    if (newRows === oldRows) {
      e.target.rows = newRows;
    }
    this.setState({
      message: e.target.value,
      rows: newRows
    });
  }
  handleSubmit(e) {
    e.preventDefault();
    if (this.state.message.length < 1) {
      window.alert("Please type a message before sending.");
    } else {
      this.props.sendMessage(this.props.activeChat, this.state.message);
      this.setState({
        message: "",
        rows: 1
      });
    }
  }
  handleKeyPress(e) {
    console.log("enter");
    if (e.key === "Enter") {
      this.handleSubmit(e);
    }
  }
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <StyledInput px={4} py={3} flexWrap="noWrap">
          <Box px={2} width={[1 / 10, 1 / 10, 0.5 / 8]}>
            <Avatar />
          </Box>
          <Box
            px={2}
            className="chatText"
            width={[9 / 10, 9 / 10, 7 / 8]}
            style={{ position: "relative" }}
          >
            <div className="inputWrapper">
              <textarea
                onKeyPress={this.handleKeyPress}
                className="chatInput"
                onChange={this.handleChange}
                value={this.state.message}
                rows={this.state.rows}
              />
              <Button
                raised
                className="button"
                onClick={this.handleSubmit}
                icon={
                  <Icon
                    type="sendmessage"
                    width={20}
                    height={20}
                    color="#fff"
                  />
                }
              >
                Send
              </Button>
            </div>
          </Box>
        </StyledInput>
      </form>
    );
  }
}
const Avatar = styled.div`
  height: 40px;
  width: 40px;
  border-radius: 50%;
  background: url(https://randomuser.me/api/portraits/women/68.jpg);
  background-size: contain;
`;
const StyledInput = styled(Flex)`
  background: #fff;
  position: absolute;
  border-top: 1px solid ${props => props.theme.borderColorOne};
  bottom: 0;
  right: 0;
  width: 100%;
  padding: 30px 50px 60px 50px;
  @media screen and (max-width: 767px) {
    padding: 30px 20px 60px 20px;
    .userImage {
      position: relative;
      top: 10px;
    }
  }
  .chatInput {
    box-sizing: border-box;
    border: 1px solid #e7e7e8;
    border-radius: 10px;
    background-color: #ffffff;
    box-shadow: 0 15px 30px 0 rgba(16, 27, 79, 0.15);
    padding: 18px 75px 18px 18px;
    width: 100%;
    color: #868e96;
    font-size: 16px;
    resize: none;
  }
  .userImage {
    height: 40px;
    width: 40px;
    border-radius: 50%;
    background: linear-gradient(135deg, #01d3ff 0%, #00a8ff 100%);
  }
  .button {
    position: absolute;
    right: 18px;
    bottom: 17px;
    display: flex;
    align-items: center;
    color: #fff;
    font-weight: 600;
  }
`;
